<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Model Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/config.html
 */
class CI_Model {

	/**
	 * Constructor
	 *
	 * @access public
	 */
	function __construct()
	{
		log_message('debug', "Model Class Initialized");
	}

	/**
	 * __get
	 *
	 * Allows models to access CI's loaded classes using the same
	 * syntax as controllers.
	 *
	 * @param	string
	 * @access private
	 */
	function __get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}
        
        public function get($q)
        {
            $query = $this->db->query($q);

            $records = array();

            foreach ($query->result() as $row)
            {
                $records[] = $row;
            }

            return $records;
        }

        public function find($table, $fields = array(), $conditions = array(), $groupBy = array(), $orderBy = array(), $start = null, $limit = null)
        {
            if (empty($fields))
            {
                $fields = "*";
            }

            if ($fields && is_array($fields))
            {
                $fields = implode(",", $fields);
            }

            if ($conditions && is_array($conditions))
            {
                $conditions = get_where($conditions);
            }

            if ($groupBy && is_array($groupBy))
            {
                $groupBy = implode(",", $groupBy);
            }

            if ($orderBy && is_array($orderBy))
            {
                $temp = array();
                foreach($orderBy as $f => $dir)
                {
                    $temp[] = "$f $dir";
                }

                $orderBy = implode(",", $temp);
            }

            //building query
            $q = "SELECT $fields FROM `" . $table . "`";

            if ($conditions)
            {
                $q .= " WHERE $conditions";
            }

            if ($groupBy)
            {
                $q .= " GROUP BY $groupBy";
            }

            if ($groupBy)
            {
                $q .= " ORDER BY $orderBy";
            }

            if (!is_null($start) && !is_null($limit))
            {
                $q .= " LIMIT $start,$limit";
            }

            return obj_to_array($this->get($q));
        }

        public function findList($table, $key_field, $val_field, $conditions = array(), $orderBy = array())
        {
            $records = obj_to_array($this->find($table, array($key_field, $val_field), $conditions, array(), $orderBy));

            $list = array();

            foreach($records as $record)
            {
                $list[$record[$key_field]] = $record[$val_field];
            }

            return $list;
        }

        public function save($table, $data)
        {
            $fields = $values = array();
            foreach($data as $f => $v)
            {
                $fields[] = "`" . trim($f) . "`";
                $values[] = "'" . trim($v) . "'";
            }

            $fields = implode(",", $fields);
            $values = implode(",", $values);

            $q = "INSERT INTO $table ($fields) VALUES($values)";
            $this->db->query($q);
        }
}
// END Model Class

/* End of file Model.php */
/* Location: ./system/core/Model.php */