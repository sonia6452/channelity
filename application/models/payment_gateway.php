<?php
/* 
 * @created 03-03-2017
 * @author Hardeep
 */

class Payment_gateway extends CI_Model 
{
    public $table_name = "app_payment_gateways";
    
    function __construct()
    {
        parent::__construct();
    }
    
    public function get($q)
    {
        $query = $this->db->query($q);

        $records = array();
        
        foreach ($query->result() as $row)
        {
            $records[] = $row;
        }
        
        return $records;
    }
    
    public function find($fields = array(), $conditions = array(), $groupBy = array(), $orderBy = array())
    {
        if (empty($fields))
        {
            $fields = "*";
        }
        
        if ($fields && is_array($fields))
        {
            $fields = implode(",", $fields);
        }
        
        if ($conditions && is_array($conditions))
        {
            $c = count_dimension($conditions);
            if ($c == 1) 
            {
                //single dimensions
                $temp = $conditions;
                $conditions = array();
                foreach($temp as $f => $v)
                {
                    $conditions["AND"][] = array(
                        "field" => $f, "value" => $v
                    );
                }
            }
            
            $conditions = get_where($conditions);
        }
        
        if ($groupBy && is_array($groupBy))
        {
            $groupBy = implode(",", $groupBy);
        }
        
        if ($orderBy && is_array($orderBy))
        {
            $temp = array();
            foreach($orderBy as $f => $dir)
            {
                $temp[] = "$f $dir";
            }
            
            $orderBy = implode(",", $temp);
        }
        
        //building query
        $q = "SELECT $fields FROM `" . $this->table_name . "`";
        
        if ($conditions)
        {
            $q .= " WHERE $conditions";
        }
        
        if ($groupBy)
        {
            $q .= " GROUP BY $groupBy";
        }
        
        if ($groupBy)
        {
            $q .= " ORDER BY $orderBy";
        }
        
        return $this->get($q);
    }
      
    public function findList($key_field, $val_field, $conditions = array(), $orderBy = array())
    {
        $records = obj_to_array($this->find(array($key_field, $val_field), $conditions, array(), $orderBy));
        
        $list = array();
        
        foreach($records as $record)
        {
            $list[$record[$key_field]] = $record[$val_field];
        }
        
        return $list;
    }
}

