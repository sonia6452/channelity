<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('IS_ACTIVATE_SUPER_ADMIN_NEW_THEME', '1'); 

/* End of file constants.php */
/* Location: ./application/config/constants.php */

define("SITE_NAME", 'x724');
define("PAYMENT_GATEWAY_PAYPAL", 1);
define("PAYMENT_GATEWAY_GOOGLE_WALLET", 2);
define("PAYMENT_GATEWAY_PAYTRAIL", 3);
define("PAYMENT_GATEWAY_STRIPE", 4);

define("PAYMENT_GATEWAY_FIELDS_PUBLISHER_API_KEY", 4);
define("PAYMENT_GATEWAY_FIELDS_SECRET_API_KEY", 5);

//From Hardeep account
define("STRIPE_PUBLISHER_API_KEY", "pk_test_kgmnJjBquESPMOv9WEPDa1VH");
define("STRIPE_SECRET_API_KEY", "sk_test_w8i2ltMhDhRVDbo7IyC2VzT0");