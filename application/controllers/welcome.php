<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->helper('url');
		$this->load->view('header');
		$this->load->view('welcome_message');
		$this->load->view('footer');
	}
        
        public function set_city_region()
        {
            ini_set("max_execution_time", 60 * 60 * 3);
            ini_set("memory_limit", "1024M");
        
            $q = "RENAME TABLE app_cities TO app_cities_big";
            $this->db->query($q);
            
            $q = "CREATE TABLE IF NOT EXISTS `app_cities` (
                    `city_id` int(11) unsigned NOT NULL,
                    `country_id` smallint(5) unsigned NOT NULL,
                    `region_id` int(11) unsigned NOT NULL,
                    `city_name` varchar(255) NOT NULL,
                    `lat` varchar(255) NOT NULL,
                    `long` varchar(255) NOT NULL,
                    `city_key` varchar(255) NOT NULL,
                    `is_active_s` enum('Y','N') NOT NULL,
                    `city_order` int(11) NOT NULL
                  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;";
            $this->db->query($q);
            
            $q = "ALTER TABLE `app_cities`
                  ADD PRIMARY KEY (`city_id`), ADD KEY `country_id` (`country_id`), ADD KEY `region_id` (`region_id`), ADD KEY `country_id_2` (`country_id`), ADD KEY `region_id_2` (`region_id`);";
            $this->db->query($q);
            
            $q = "ALTER TABLE `app_cities` MODIFY `city_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;";
            $this->db->query($q);
            
            $q = "INSERT INTO app_cities SELECT * FROM app_cities_old;";
            $this->db->query($q);
            
            $q = "INSERT INTO app_cities
            (country_id, region_id, city_name, lat, `long`, is_active_s, city_order) 
            SELECT country_id, region_id, city_name, lat, `long`, is_active_s, city_order FROM app_cities_big where country_id NOT IN (select distinct country_id from app_cities);";
            $this->db->query($q);
            
            
            $q = "RENAME TABLE app_regions TO app_regions_big";
            $this->db->query($q);
            
            $q = "CREATE TABLE IF NOT EXISTS `app_regions` (
                `region_id` int(11) unsigned NOT NULL,
                `country_id` smallint(5) unsigned NOT NULL,
                `region_code` varchar(100) NOT NULL,
                `region_name` varchar(100) NOT NULL,
                `is_actives` enum('Y','N') NOT NULL,
                `region_order` int(11) NOT NULL
              ) ENGINE=InnoDB AUTO_INCREMENT=4108 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;";
            $this->db->query($q);
            
            $q = "ALTER TABLE `app_regions` ADD PRIMARY KEY (`region_id`), ADD KEY `country_name` (`country_id`,`region_name`), ADD KEY `country_id` (`country_id`);";
            $this->db->query($q);
            
            $q = "ALTER TABLE `app_regions` MODIFY `region_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;";
            $this->db->query($q);
            
            $q = "INSERT INTO app_regions SELECT * FROM app_regions_old;";
            $this->db->query($q);
            
            $q = "INSERT INTO app_regions
                (country_id, region_code, region_name, is_actives, region_order) 
                SELECT country_id, region_code, region_name, is_actives, region_order FROM app_regions_big where country_id NOT IN (select distinct country_id from app_regions);";
            $this->db->query($q);
            
            echo 1; exit;
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */