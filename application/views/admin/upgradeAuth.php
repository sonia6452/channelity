<!DOCTYPE html>
<html lang="en-US" dir="ltr">
<head>
<!--<meta charset="utf-8">-->
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15" />
<!--meta http-equiv="Content-Type" content="text/html; charset=utf-8" /-->
<meta content="initial-scale=1.6; maximum-scale=1.0; width=device-width;" name="viewport">
<title>Oops... &bull; <?php echo SITE_NAME; ?></title>;
<!--link href="http://register.bookient.com/asset/front_css/defult_headerFront.css" rel="stylesheet" type="text/css" /-->
<link href="<?php echo base_url(); ?>/min/?f=asset/front_css/defult_headerFront.css,asset/front_css/rating_star.css" rel="stylesheet" type="text/css" />
<?php if(IS_ACTIVATE_SUPER_ADMIN_NEW_THEME): ?>
    <link href="<?php echo base_url(); ?>min/?f=asset/front_css/defult_headerFront.css,asset/front_css/front_common_new.css" rel="stylesheet" type="text/css" />
<?php endif; ?>
<link rel="shortcut icon" href="<?php echo base_url(); ?>asset/front_image/favicon.ico" type="image/vnd.microsoft.icon" />
<script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>

<script type="text/javascript">
var BASE_URL = "<?php echo base_url(); ?>index.php"; 
var SITE_URL = '<?php echo base_url(); ?>';
</script>

</head>
<body data-spy="scroll" data-target=".bs-docs-sidebar" data-twttr-rendered="true">
<div id="dynamic_popup_id">
<div class="wrapper">

<div>
  <hgroup id="header">
    <h1 class="logoCon">
        <a class="block" title="<?php echo SITE_NAME; ?>" href="<?php echo base_url(); ?>">
            <?php if(IS_ACTIVATE_SUPER_ADMIN_NEW_THEME) : ?>
                <img src="<?php echo base_url(); ?>images/new_logo.png" alt="logo" width="60"/>
            <?php else: ?>
                <img src="<?php echo base_url(); ?>images/logo.png" alt="logo" />
            <?php endif; ?>
        </a>
    </h1>
    <div class="UserCon">
          </div>
    <div class="clearfix"></div>
  </hgroup>
  <div class="clearfix"></div>
  <nav class="navbar" style="position:relative;">
   <button type="button" class="btn btn-navbar" data-toggle="collapse" >
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
   </button>
   <div class="nav-collpas">
    <ul class="menu" id="lang_log">   
            <li id="menu"><a>&nbsp;</a></li>             
    </ul>
  <div class="clearfix"></div>
    <div id="cus_menu">

    </div>

    </div>
 <div class="clearfix"></div>
  </nav>
  <div class="clearfix"></div>
</div>


<div class="alertContent"><?php echo $alertMsg; ?></div>
<style>

.alertContent{
	background: none repeat scroll 0 0 #f1f1f1;
    border: 10px solid #ccc;
    border-radius: 2em;
    font-size: 2em;
    left: 33%;
    margin: 0 auto;
    padding: 20px;
    position: absolute;
    text-align: center;
    top: 40%;
    width: 33%;
}
	
</style>

<?php if(IS_ACTIVATE_SUPER_ADMIN_NEW_THEME) : ?>
<style>
    body {
        background: #fff;
    }
    #header {
        background: #f2f2f2 none repeat scroll 0 0;
        float: left;
        width: 100%;
    }
    #header h1{
        padding: 0px;
    }
    .nav-collpas {
        display: none;
    }
    .footer {
        margin-top: 0px;
        background: #193E6A;
    }
    .footer .twelve {
        padding-top: 0px;
    }
    .alertContent {
        border: 3px solid #ccc;
        border-radius: 0px;
    }
</style>
<?php endif; ?>

</div>
<div class="footer">
  <footer class="row">
    <div class="twelve columns">
    	<ul>
        	<li><a>&nbsp;</a></li>
        </ul>
        <p>&copy; Copyright <a href="<?php echo base_url(); ?>" target="_blank">x724.com</a> 2017. All rights reserved.</p>
   
        <div class="social-icons">
           
        </div>
  
    </div>
  </footer>
</div>
</div>
</body>
</html>
