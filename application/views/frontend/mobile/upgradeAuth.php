<!DOCTYPE html> 
<html>
<head>
	<!--meta charset="utf-8"-->
    <meta charset="iso-8859-15">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title><?php echo SITE_NAME; ?></title> 
	<link rel="shortcut icon" href="<?php echo base_url();?>asset/front_image/favicon.ico" type="image/vnd.microsoft.icon" />
	<link rel="stylesheet" href="<?php echo base_url();?>asset/mobile_css/jquery.mobile-1.2.1.min.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>asset/mobile_css/jquery.ui.datepicker.mobile.css" /> 
	<link rel="stylesheet" href="<?php echo base_url();?>asset/mobile_css/style.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>asset/mobile_css/lightBox.css" />
	<script type="text/javascript" src="<?php echo base_url();?>asset/mobile_js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>asset/mobile_js/jquery.mobile-1.2.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>asset/mobile_js/mobile_index.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>asset/mobile_js/jQuery.ui.datepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>asset/mobile_js/jquery.ui.datepicker.mobile.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>asset/mobile_js/login.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>asset/mobile_js/lightBox.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>asset/mobile_js/booking.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>asset/mobile_js/reviewAndAppointments.js"></script>

</head> 
<body> 
<div data-role="page"><!-- main contener --><div data-role="header" data-position="fixed" data-transition="none" class="mainHeader" data-theme="b">
	<?php if(IS_ACTIVATE_SUPER_ADMIN_NEW_THEME) : ?>
            <img src="<?php echo base_url(); ?>images/new_logo.png" alt="logo" width="60" />            
        <?php else: ?>
            <img src="<?php echo base_url(); ?>images/logo.png" alt="logo" />
        <?php endif; ?>
<div style="clear:both"></div>
<div class="ui-navbar ui-mini" data-role="navbar" role="navigation" data-iconpos="right" style="float:left;width: 100%;margin: 0;padding: 0;">
	<ul style="float:left;width: 100%;margin: 0;padding: 0;">
		<li><a class="topMenul">&nbsp; </a></li>
	</ul>
</div><!-- /navbar -->
</div><!-- /header -->
<div data-role="content" id="mainContent">	
<!--##########################################################################################-->
<!--############################ Mani Content Start ##########################################-->
<!--##########################################################################################-->

<div class="ui-body ui-body-c serviceContent" id="activeService" >	
		<label class="clActiveService" style="text-align: center;"><h2><?php echo $alertMsg; ?></h2></label>	 
</div>

<!--##########################################################################################-->
<!--############################## Mani Content End ##########################################-->
<!--##########################################################################################-->
</div><!-- /content -->

<?php if(IS_ACTIVATE_SUPER_ADMIN_NEW_THEME) : ?>
<style>
    .mainHeader.ui-bar-b {
        background: #F2F2F2;
    }
    .ui-grid-solo li {
        list-style-type: none !important;
    }
    .ui-grid-solo li a {
        float: left !important;
        width: 100% !important;
    }
    .ui-header .ui-title, .ui-footer .ui-title {
        overflow: visible;
        display: block !important;
        white-space: pre-line;
        font-size: 14px;
    }
    .mainFooter {
        position: fixed !important;
        bottom: 0px !important;
    }
</style>
<?php endif; ?>
<div data-role="footer" data-position="fixed" data-transition="none" class="mainFooter" data-theme="b"
     style="float:left;width: 100%;margin: 0;padding: 0;bottom: 0px;">
	<div data-role="navbar" data-iconpos="left" style="float:left;width: 100%;margin: 0;padding: 0;">
		<ul style="float:left;width: 100%;margin: 0;padding: 0;">
			<li><a>&nbsp;</a></li>
		</ul>
	</div><!-- /navbar -->
	<h1>&copy; Copyright <a href="<?php echo base_url(); ?>" target="_blank">x724.com</a> 2017. All rights reserved.</h1>
</div><!-- /footer -->
	
</div><!-- /main contener -->

</body>
</html>