<style>
    .dialog .head
    {
        background: #548bb8;
        color : #FFF;
        padding: 0.6em 1em;
        font-weight: bold;
        margin-bottom: 5px;
    }
    
    .dialog .body
    {
        max-height: 430px;
        overflow: auto;
        
        padding-top : 0.5em;
        padding-bottom : 0.5em;
        
        border-top : 1px solid #000;
        border-bottom : 1px solid #000;
    }
    .dialog .footer
    {
        margin-top : 5px;
    }
    
    ul.list-of-link
    {
        list-style-type: none;
        margin: 35px auto;
        width: 92%;
    }
    
    ul.list-of-link li
    {
        border: 1px solid;
        display: inline-block;
        margin-right: 15px;
        cursor: pointer;
    }
    .list-of-link > li a img {
        height: 60px;
        padding: 10px;
    }
</style>
<?php 
    $icons = array(PAYMENT_GATEWAY_PAYPAL => 'paypal-icon.jpg', PAYMENT_GATEWAY_GOOGLE_WALLET => 'google-wallet-icon.png', 
                PAYMENT_GATEWAY_PAYTRAIL => 'paytrail-icon.jpg', PAYMENT_GATEWAY_STRIPE => 'stripe-icon.png');
?>
<div class="dialog">
    <div class="head">
        <?= $this->lang->line('choose_payment_method'); ?>
    </div>    
    <div class="body">
        <?php if (empty($payment_gateway_list)) : ?>
        <h3>No Payamnet Gateway enabled.</h3>
        <?php else : ?>
        <ul class="list-of-link">            
            <?php        
            foreach($payment_gateway_list as $result):?>
            <li>
                <a data-payment-gateway-id="<?php echo $result->payment_gateways_id; ?>" class="payment-gateway-chooser">
                    <img src="<?php echo base_url() . 'asset/' . $icons[$result->payment_gateways_id];?>" height="20px" title="<?php echo $result->payment_gateways_name; ?>"/>
                </a>
            </li>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>
    </div>
</div>

<!-- storing previus post -->
<script type="text/javascript">
    var post_data = JSON.parse('<?= json_encode($_POST) ?>');
</script>