
<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>x724.com</title>	
    <!-- font link -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i,800" rel="stylesheet"> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Bootstrap -->
    <link href="/html/css/bootstrap.min.css" rel="stylesheet">
     <!-- font css -->
    <link rel="stylesheet" type="text/css" href="/html/css/ionicons.css" media="all" />
	<!-- Animate css -->
	<link href="/html/css/animate.min.css" rel="stylesheet">
    <!--banner slider-->
	<link href="/html/css/flickerplate.css"  type="text/css" rel="stylesheet">
    <!--/banner slider-->
	 <!-- custom css -->
	<link href="/html/css/style.css" rel="stylesheet">
	
    <style>
        .text-input-red
        {
            border: 1px solid red;
        }
        .feild-box3 
        {
            width: 170px;
            position: relative;
            float: left;
        }
        .error
        {
            float: left;
        }
        
        #other_profession
        {
            display: none;
        }
    </style>
  </head>
<body>
<?php include('registration.js.php'); ?>
<?php 
$exp			=	explode('//',$current_url);
$exp1			=	explode('/',$exp[1]);
$url			=	$exp1[0];
?>
    <script>
        var BASE_URL ="<?php echo site_url(); ?>";
        var SITE_URL ="<?php echo base_url(); ?>";
    </script>
<div class="wrapper">
	<header>
	<div class="navbar navbar-default navbar-fixed-top scrollspy_menu navbar__initial" role="navigation">
		 <div class="container text-center">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand nav-logo" href="#home">
					  <img class="img-responsive" src="/html/images/logo.png" alt="logo">
					</a>                    
				</div>
				<div class="navbar-collapse collapse">					
					<ul class="nav navbar-nav navbar-right mainnav">
						<li><a href="#">Features</a></li>
						<li><a href="#">Support</a></li>
						<li><a href="#">Integrations</a></li>
					</ul>
				</div>				
		</div>	
	</div>
</header>  <!--/header outer end-->

	<div class="banner">
    <div class="flicker-example">
    	<div class="banner-caption">
        	<div class="container">
            	<div class="row">
                	<div class="col-xs-12">
                		<div class="create-form">
                        	<h3>Create An Account</h3>
                            <h4><span><?php echo $this->global_mod->db_parse($this->lang->line('reg_heading_appointysiteaddress')); ?></span></h4>
                            <form id="form-sign-up" class="from_registred" action="" method="post">
                            	<div class="form-row">
                                	<label><?php echo $this->global_mod->db_parse($this->lang->line('reg_username')); ?>:</label>
                                    <div class="feild-box">
                                    	<div class="feild-box3">
                                            <input name="user_name" type="text" class="form-control required username" placeholder="" required autofocus>
                                        </div>
                                        <span>.<?php echo $url; ?></span> 
                                        <div class="error" id="userErr"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                
                                <div class="form-row">
                                	<label><?php echo $this->global_mod->db_parse($this->lang->line('reg_email')); ?>:</label>
                                    <div class="feild-box">
                                        <input name="email" type="text" id="register-email" class="form-control required email" placeholder="" required autofocus>
                                    </div>
                                    <div class="error" id="err"></div>
                                    <div class="clearfix"></div>
                                </div>
                                
                                <div class="form-row">
                                	<label><?php echo $this->lang->line('reg_password'); ?></label>
                                    <div class="feild-box"><input name="password1" type="password" id="password-1" class="form-control required password" placeholder="" required autofocus></div>
                                    <div class="clearfix"></div>
                                </div>
                
                                <div class="form-row">
                                	<label><?php echo $this->lang->line('reg_repeat_password'); ?>:</label>
                                    <div class="feild-box"><input name="password2" type="password" id="password-2" class="form-control required password" placeholder="" required autofocus></div>
                                    <div class="clearfix"></div>
                                </div>
                                
                                <h4><span><?php echo $this->global_mod->db_parse($this->lang->line('reg_heading_administratoraccount')); ?></span></h4>
                                
                                <div class="form-row">
                                	<label><?php echo $this->lang->line('reg_name'); ?></label>
                                    <div class="feild-box">
                                    	<div class="col1"><input name="firstname" type="text" id="firstname" class="form-control required" placeholder="<?php echo $this->lang->line('reg_first_name')?>" required autofocus></div>
                                        <div class="col2"><input name="lastname" type="text" id="lastname" class="form-control required" placeholder="<?php echo $this->lang->line('reg_last_name')?>" required autofocus></div>
                                    </div>
                                    <div  class="clearfix"></div>
                                </div>
                                
                                <div class="form-row">
                                	<label><?php echo $this->lang->line('reg_profession'); ?>:</label>
                                    <div class="feild-box">
                                   	  <div class="feild-box2"> 
                                        <div class="select-style">
                                            <select class="tu" id="profession" name="profession">
                                                <option value="">Please Select</option>
                                                <?php foreach($profession as $key => $value) :?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                <?php endforeach; ?>
                                            </select>
										</div>
                                              
                                            <input name="other_profession" id="other_profession" type="text" disabled="true" class="form-control required" placeholder="<?php echo $this->lang->line('reg_other_profession')?>" >
                                      </div>
                                        <span><a href="javascript:void(0);" id="other-opener"><?php echo $this->lang->line('reg_other')?></a></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                
                                
                                
                                <div class="form-row">
                                	<label><?php echo $this->lang->line('reg_country'); ?>:</label>
                                    <div class="feild-box">
                                    	<div class="select-style">
                                            <select class="tu" id="country" name="country">
                                                <option value="">Please Select</option>
                                                <?php foreach($country as $key => $value) :?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                <?php endforeach; ?>
                                          </select>
										</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                
                                <div class="form-row">
                                    <input type="button"  id="btn-submit-register" value="Create An Account" alt="Go" class="btn-submit-login img-swap btn btn-lg btn-primary btn-block"  /> 
                                </div>
                            </form>
                        </div>
                        <h2>POWERFUL. FREE. SCHEDULING.</h2>
                		<p>Manage appointments, customers, staff, schedules and more -100% free!</p>
                    </div>
                </div>
            </div>
        </div>
        <ul>
            <li data-background="/html/images/slider1.jpg"></li>
            <li data-background="/html/images/slider1.jpg"></li>
            <li data-background="/html/images/slider1.jpg"></li>
        </ul>
	</div>
</div>  <!--/banner outer end-->




	
</div>  <!--/banner outer end-->


<!--Our Features outer-->
   <div class="ourfeatures-outer">
   		<div class="container">
        	<div class="row clearfix">
            	<h3>Our Features</h3>
                
                <div class="wow fadeInLeft" data-wow-duration="0.4s" data-wow-delay="0.6s">
                
               		<div class="col-sm-4">
                        <div class="features-info">
                            <div class="features-icon"> <img src="/html/images/online-icon.png" alt="Online Appointments"> </div>
                            <h4>Online Appointments</h4>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
                        </div>
                	</div>
                
                	<div class="col-sm-4">
                        <div class="features-info">
                            <div class="features-icon"> <img src="/html/images/staff-icon.png" alt="Online Appointments"> </div>
                            <h4>Staff Scheduling</h4>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
                        </div>
                	</div>
                
                	<div class="col-sm-4">
                        <div class="features-info">
                            <div class="features-icon"> <img src="/html/images/customer-icon.png" alt="Online Appointments"> </div>
                            <h4>Customer Reviews</h4>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
                        </div>
                	</div>
                    
                    <div class="col-sm-4">
                        <div class="features-info">
                            <div class="features-icon"> <img src="/html/images/reminder-icon.png" alt="Online Appointments"> </div>
                            <h4>Auto Reminders</h4>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
                        </div>
                	</div>
                
                	<div class="col-sm-4">
                        <div class="features-info">
                            <div class="features-icon"> <img src="/html/images/mobile-icon.png" alt="Online Appointments"> </div>
                            <h4>Mobile Ready</h4>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
                        </div>
                	</div>
                
                	<div class="col-sm-4">
                        <div class="features-info">
                            <div class="features-icon"> <img src="/html/images/booking-icon.png" alt="Online Appointments"> </div>
                            <h4>Self Booking</h4>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
                        </div>
                	</div>
                    
                    </div>
                
            </div>
        </div>
   </div>
<!--/Our Features outer end-->


<!--Self-service outer start-->
	<div class="selfservice-outer">
    	<div class="container">
        	<div class="row">
            	<div class="col-xs-12">
            	<h3>Self-service scheduling from</h3>
                <p>Enhance your website's appeal by embedding the free Setmore booking page directly on your site. Let your webdesign draw your customers in, we'll fill up your appointment calendar.</p>
                <div class="account-box"> <a href="#" class="btn btn-lg btn-primary btn-block">Create a Account</a></div>
                <div class="laptop-outer wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.4s">
                	<img src="/html/images/laptop.png" alt="" class="img-responsive"> 
                </div>
                </div>
            </div>
        </div>
    </div>
<!--/Self-service outer end-->



<!--Integrate outer start-->
	<div class="integrate-outer">
    	<div class="container">
        	<div class="row">
            	<div class="col-xs-12">
            	<h3>Integrate with your existing apps</h3>
                <ul>
                	<li> 
                    	<div class="logo-icon"> <img src="/html/images/facebook.png" alt=""> </div> 
                    </li>
                    <li> 
                    	<div class="logo-icon"> <img src="/html/images/google.png" alt=""> </div> 
                    </li>
                    <li> 
                    	<div class="logo-icon"> <img src="/html/images/wordpress.png" alt=""> </div> 
                    </li>
                    <li> 
                    	<div class="logo-icon"> <img src="/html/images/mailchamp.png" alt=""> </div> 
                    </li>
                    <li> 
                    	<div class="logo-icon"> <img src="/html/images/strip.png" alt=""> </div> 
                    </li>
                    <li> 
                    	<div class="logo-icon"> <img src="/html/images/paypal.png" alt=""> </div> 
                    </li>
                    <li> 
                    	<div class="logo-icon"> <img src="/html/images/slack.png" alt=""> </div> 
                    </li>
                    <li> 
                    	<div class="logo-icon"> <img src="/html/images/zendesk.png" alt=""> </div> 
                    </li>
                    <li> 
                    	<div class="logo-icon"> <img src="/html/images/joomla.png" alt=""> </div> 
                    </li>
                    <li> 
                    	<div class="logo-icon"> <img src="/html/images/wix.png" alt=""> </div> 
                    </li>
                    <li> 
                    	<div class="logo-icon"> <img src="/html/images/weebly.png" alt=""> </div> 
                    </li>
                    <li> 
                    	<div class="logo-icon"> <img src="/html/images/zapier.png" alt=""> </div> 
                    </li>
                </ul>
                </div>
            </div>
        </div>
    </div>
<!--/Integrate outer end-->


<!--app outer start-->
	<div class="app-outer">
    	<div class="container">
        	<div class="row clearfix">
            	<div class="col-sm-6 wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.4s">
                	<img src="/html/images/app.png" alt="" class="img-responsive"> 
                </div>
                <div class="col-sm-6">
                	<h3>iOS & Android Ready</h3>
                    <p>Your mobile device is an extension of your business. Download our free apps for iOS and Android, and stay connected to your business, everywhere.</p>
                    <ul>
                    	<li> <img src="/html/images/app-store.png" alt=""> </li>
                        <li> <img src="/html/images/google-pay.png" alt=""> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<!--/app outer end-->



<!--free outer start-->
	<div class="free-outer">
    	<div class="container">
        	<div class="row">
            	<div class="col-xs-12">
                	<h3>It’s FREE! No strings attached</h3> 
                    <p>Sign up to experience easy, smooth and efficient appointment scheduling. Get your FREE account TODAY!</p>
                    <div class="account-box"> <a href="#" class="btn btn-lg btn-primary btn-block">Create a Account</a></div>
                </div>
            </div>
        </div>
    </div>
<!--/free outer end-->


<!--footer outer start-->
    <footer>
        <div class="container">
            <div class="row clearfix">
                <div class="col-xs-12">
                    <div class="footer-row1 clearfix">
                        <div class="col-sm-6">
                            <ul class="footer-list1">
                                <li> <img src="/html/images/footer-logo.png" alt=""> </li>
                                <li> Love your Finacial Life! </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="footer-list2">
                                <li><a href="#">Contact</a></li>      
                                <li><a href="#">Team</a></li>      
                                <li><a href="#">Privacy</a></li>      
                                <li><a href="#">Imprint</a></li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="footer-row2 clearfix">
                        <div class="col-sm-6">
                            <p>©2017x724.all right reserved</p>
                        </div>
                        <div class="col-sm-6">
                            <ul>
                                <li><a href="#">&#xf231;</a></li>
                                <li><a href="#">&#xf243;</a></li>
                                <li><a href="#">&#xf351;</a></li>
                                <li><a href="#">&#xf235;</a></li>
                            </ul>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </footer>
<!--/footer outer end-->

















<!-- jQuery (JavaScript plugins) -->
<script src="/html/js/bootstrap.min.js"></script>
<script src="/html/js/wow.min.js"></script>
<script src="/html/js/jquery.waypoints.min.js"></script>


<!-- owl js-->
<script src="/html/js/owl.carousel.min.js"></script>
<script src="/html/js/main.js"></script>


<!--banner slider-->  
<script src="/html/js/hammer-v2.0.3.js"></script>
<script src="/html/js/flickerplate.js"></script>
<script src="/html/js/custom.js"></script>
<!--/banner slider--> 

<script type="text/javascript">
    $("#other-opener").click(function()
    {
        $("input#other_profession").removeAttr("disabled").show();
    })
</script>

</body>
</html>