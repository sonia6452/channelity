<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title>Rating</title>

<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>asset/admin_css/admin_common.css" />
<?php if(IS_ACTIVATE_SUPER_ADMIN_NEW_THEME): ?>
    <link href="<?php echo base_url(); ?>min/?f=asset/front_css/defult_headerFront.css,asset/front_css/front_common_new.css" rel="stylesheet" type="text/css" />
<?php endif; ?>

</head>
<body>
<div class="wrapper">

<div class="maincontainer">
<div class="header">
    <h1 class="logoCon">
        <a class="block" title="Pardco" href="<?php echo base_url(); ?>">
            <?php if(IS_ACTIVATE_SUPER_ADMIN_NEW_THEME) : ?>
                <img src="<?php echo base_url(); ?>images/new_logo.png" alt="logo" width="60"/>
            <?php else: ?>
                <img src="<?php echo base_url(); ?>images/logo.png" alt="logo" />
            <?php endif; ?>
        </a>
    </h1>
</div>
<div class="clearfix"></div>
</div>

<?php if(IS_ACTIVATE_SUPER_ADMIN_NEW_THEME) : ?>
<style>
    body {
        background: #fff;
    }
    .header {
        background: #f2f2f2 none repeat scroll 0 0;
        border-bottom: 1px solid #ddd;
        float: left;
        width: 100%;
    }
    .header h1{
        padding: 0px;
    }
    .er-msg-div {
        border-radius: 0px;
    }
</style>
<?php endif; ?>


