-- MySQL dump 10.15  Distrib 10.0.27-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: bookient
-- ------------------------------------------------------
-- Server version	10.0.27-MariaDB-0+deb8u1
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'bookient'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`bookient`@`localhost` FUNCTION `conver_epoc`(
	in_time_stamp INT, 
	my_ind char(1)
) RETURNS int(11)
BEGIN
   DECLARE dist INT(11);
   DECLARE res INT(11) DEFAULT 0;
   SET dist = TIMESTAMPDIFF(second,utc_timestamp(), now());
   IF my_ind = '+' THEN
       SET res = in_time_stamp-dist;
   ELSE
          SET res = in_time_stamp+dist;
   END IF;
   RETURN res;
   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`bookient`@`localhost` FUNCTION `getCustomerId`(in_customer_id INT) RETURNS int(10) unsigned
BEGIN

 
 RETURN in_customer_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`bookient`@`localhost` PROCEDURE `bookingDetails`(
	IN locl_admin INT,
	IN time_zone varchar(15),
	IN in_string_data varchar(255),
	IN in_start_data varchar(255))
BEGIN

   DECLARE li_booking_id,
       li_customer_id INT;
   DECLARE lt_booking_date_time DATETIME;
   DECLARE ls_total_tax varchar(100);
   DECLARE ls_discount_amnt varchar(100);
   DECLARE ls_grand_total varchar(100);
   DECLARE ls_prepayment_amount  varchar(100);
   DECLARE ls_grand_sub_cost varchar(100);
   DECLARE ls_discount_coupon_dtls varchar(100);
   DECLARE ls_tax_dtls_arr varchar(100);
   DECLARE ls_prepayment_details varchar(100);
   DECLARE ls_comment varchar(255);
   DECLARE first_cursor boolean;
   DECLARE temp_booking_id INT;
       DECLARE ls_currency_id INT;
       
   
   DECLARE bookingTab_cursor CURSOR FOR


           SELECT    
               booking_id,
               customer_id,
                               currency_id,
               ADDTIME(booking_date_time,time_zone) booking_date_time,
               booking_sub_total,
               booking_disc_amount,
               booking_disc_coupon_details,
               booking_total_tax,
               booking_tax_dtls_arr,
               booking_grnd_total,
               booking_prepayment_amount,
               booking_prepayment_details,
               booking_comment
           FROM    
               app_booking
           WHERE    
               booking_is_deleted = '0'
               AND
               local_admin_id = locl_admin
           ORDER BY
               booking_id;

   DECLARE continue handler for not found set first_cursor = true;
OPEN bookingTab_cursor;

DROP TEMPORARY TABLE IF EXISTS app_temp_booking;

CREATE TEMPORARY TABLE IF NOT EXISTS `app_temp_booking` (
                       `booking_id` int(11) NOT NULL,
                       `customer_id` int(11) NOT NULL,
                                               `currency_id` int(11) NOT NULL,
                       `booking_date_time` DATETIME NOT NULL,
                       `booking_sub_total` varchar(100) NOT NULL,
                       `booking_disc_amount` varchar(100) NOT NULL,
                       `booking_disc_coupon_details` varchar(255) NOT NULL,
                       `booking_total_tax` varchar(100) NOT NULL,
                       `booking_tax_dtls_arr` varchar(255) NOT NULL,
                       `booking_grnd_total` varchar(100) NOT NULL,
                       `booking_prepayment_amount` varchar(100) NOT NULL,
                       `booking_prepayment_details` varchar(255) NOT NULL,
                       `booking_comment` varchar(255) NOT NULL,
                       `srvDtls_id` int(11) NOT NULL,
                       `srvDtls_service_id` int(11) NOT NULL,
                       `srvDtls_service_name` varchar(255) NOT NULL,
                       `srvDtls_service_cost` varchar(100) NOT NULL,
                       `srvDtls_service_duration` int(11) NOT NULL,
                       `srvDtls_service_duration_unit` varchar(10) NOT NULL,
                       `srvDtls_service_start` DATETIME NOT NULL,
                       `srvDtls_service_end` DATETIME NOT NULL,
                       `srvDtls_employee_id` int(11) NOT NULL,
                       `srvDtls_employee_name` varchar(255) NOT NULL,
                       `srvDtls_booking_status` varchar(255) NOT NULL,
                       `srvDtls_status_date` DATETIME NOT NULL,
                       `srvDtls_service_quantity` int(11) NOT NULL,
                       `srvDtls_service_description` varchar(255) NOT NULL
                             ) ENGINE=HEAP DEFAULT CHARSET=latin1;

   curloop: loop

       fetch bookingTab_cursor into
                   li_booking_id,
                   li_customer_id,
                                       ls_currency_id,
                   lt_booking_date_time,
                   ls_grand_sub_cost,
                   ls_discount_amnt,
                   ls_discount_coupon_dtls,
                   ls_total_tax,
                   ls_tax_dtls_arr,
                   ls_grand_total,
                   ls_prepayment_amount,
                   ls_prepayment_details,
                   ls_comment ;

       if first_cursor then
           close bookingTab_cursor;
           set first_cursor = false;
           leave curloop;
       end if;
       SET temp_booking_id = li_booking_id ;
BLOCK2: BEGIN

   DECLARE srvLi_srvDtls_id,
       srvLi_srvDtls_service_id INT;
       DECLARE srvLs_srvDtls_service_name varchar(255);
   DECLARE srvLs_srvDtls_service_cost varchar(255);
   DECLARE srvLi_srvDtls_service_duration INT;
   DECLARE srvLs_srvDtls_service_duration_unit varchar(100);
   DECLARE srvLt_srvDtls_service_start DATETIME;
   DECLARE srvLt_srvDtls_service_end DATETIME;
   DECLARE srvLi_srvDtls_continuation_id INT;
   DECLARE srvLi_srvDtls_employee_id INT;
   DECLARE srvLs_srvDtls_employee_name varchar(100);
   DECLARE srvLi_srvDtls_booking_status INT;
   DECLARE srvLt_srvDtls_status_date DATETIME;
   DECLARE srvLi_srvDtls_service_quantity INT;
   DECLARE srvLi_rescheduled_child_id INT;
   DECLARE srvLs_srvDtls_service_description varchar(255);
   DECLARE inner_done boolean;
       DECLARE innerCursor CURSOR FOR


           SELECT
               srvDtls_id,
               srvDtls_service_id,
               srvDtls_service_name,
               srvDtls_service_cost,
               srvDtls_service_duration,
               srvDtls_service_duration_unit,
               ADDTIME(srvDtls_service_start,time_zone) srvDtls_service_start,
               ADDTIME(srvDtls_service_end,time_zone) srvDtls_service_end,
               srvDtls_employee_id,
               srvDtls_employee_name,
               srvDtls_booking_status,
               ADDTIME(srvDtls_status_date,time_zone) srvDtls_status_date,
               srvDtls_service_quantity,
               srvDtls_service_description
           FROM
               app_booking_service_details
           WHERE
               srvDtls_booking_id  = temp_booking_id
               AND
               srvDtls_rescheduled_child_id = 0
           ORDER BY
               srvDtls_id;


DECLARE CONTINUE HANDLER FOR NOT FOUND SET inner_done = TRUE;
OPEN innerCursor ;
   cur_inner_loop: LOOP

       FETCH FROM innerCursor INTO
                   srvLi_srvDtls_id,
                   srvLi_srvDtls_service_id,
                   srvLs_srvDtls_service_name,
                   srvLs_srvDtls_service_cost,
                   srvLi_srvDtls_service_duration,
                   srvLs_srvDtls_service_duration_unit,
                   srvLt_srvDtls_service_start,
                   srvLt_srvDtls_service_end,
                   srvLi_srvDtls_employee_id,
                   srvLs_srvDtls_employee_name,
                   srvLi_srvDtls_booking_status,
                   srvLt_srvDtls_status_date,
                   srvLi_srvDtls_service_quantity,
                   srvLs_srvDtls_service_description ;  

       
       IF inner_done THEN
           CLOSE innerCursor ;
           LEAVE cur_inner_loop;
       END IF;

   INSERT INTO
       app_temp_booking (
               booking_id,
               customer_id,
                               currency_id,
               booking_date_time,
               booking_sub_total,
               booking_disc_amount,
               booking_disc_coupon_details,
               booking_total_tax,
               booking_tax_dtls_arr,
               booking_grnd_total,
               booking_prepayment_amount,
               booking_prepayment_details,
               booking_comment,
               srvDtls_id,
               srvDtls_service_id,
               srvDtls_service_name,
               srvDtls_service_cost,
               srvDtls_service_duration,
               srvDtls_service_duration_unit,
               srvDtls_service_start,
               srvDtls_service_end,
               srvDtls_employee_id,
               srvDtls_employee_name,
               srvDtls_booking_status,
               srvDtls_status_date,
               srvDtls_service_quantity,
               srvDtls_service_description
               )values(
               li_booking_id,
               li_customer_id,
               ls_currency_id,
               lt_booking_date_time,
               ls_grand_sub_cost,
               ls_discount_amnt,
               ls_discount_coupon_dtls,
               ls_total_tax,
               ls_tax_dtls_arr,
               ls_grand_total,
               ls_prepayment_amount,
               ls_prepayment_details,
               ls_comment,
               srvLi_srvDtls_id,
               srvLi_srvDtls_service_id,
               srvLs_srvDtls_service_name,
               srvLs_srvDtls_service_cost,
               srvLi_srvDtls_service_duration,
               srvLs_srvDtls_service_duration_unit,
               srvLt_srvDtls_service_start,
               srvLt_srvDtls_service_end,
               srvLi_srvDtls_employee_id,
               srvLs_srvDtls_employee_name,
               srvLi_srvDtls_booking_status,
               srvLt_srvDtls_status_date,
               srvLi_srvDtls_service_quantity,
               srvLs_srvDtls_service_description        
               );

END LOOP cur_inner_loop;
END BLOCK2;
   end loop curloop;

   SET @sql_stmt = 'SELECT ';
   IF in_start_data ='' THEN
       SET  @sql_stmt = CONCAT(@sql_stmt,' * ');
   ELSE
       SET  @sql_stmt = CONCAT(@sql_stmt,' ',in_start_data ,' ');
   END IF;
   
   SET @sql_stmt = CONCAT(@sql_stmt,' FROM app_temp_booking WHERE 1=1 ');
   IF in_string_data !='' THEN
       SET @sql_stmt =  CONCAT(@sql_stmt,' ',in_string_data);
   END IF;
   PREPARE statementExecute FROM @sql_stmt;
   EXECUTE statementExecute;

   DROP TEMPORARY TABLE IF EXISTS app_temp_booking;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`bookient`@`localhost` PROCEDURE `convToLocal`(
	IN locl_admin INT,
	IN time_zone varchar(15),
	IN in_string_data varchar(255),
	IN in_start_data varchar(255)
)
BEGIN
    DECLARE my_biz_hours_id,my_service_id,my_local_admin_id, my_employee_id, my_day_id, my_continuation_id INT;
    DECLARE my_time_from, my_time_to TIME;
    DECLARE final_time_from, final_time_to TIME;
    DECLARE final_day_id INT;
    DECLARE finished boolean;
    DECLARE cnt INT DEFAULT 0;
    DECLARE first_value int;

     
   DECLARE cur_biz CURSOR FOR

   SELECT  
       biz_hours_id,
       service_id,
       local_admin_id,
       employee_id,
       day_id,
       ADDTIME(time_from,time_zone) time_from,
       ADDTIME(time_to,time_zone) time_to ,
       continuation_id
   FROM
       app_biz_hours
   WHERE
       continuation_id=0
       AND
       local_admin_id=locl_admin
   ORDER BY
       biz_hours_id;

   
   DECLARE continue handler for not found set finished = true;
OPEN cur_biz;

DROP TEMPORARY TABLE IF EXISTS app_biz_hours_temptable;

CREATE TEMPORARY TABLE IF NOT EXISTS `app_biz_hours_temptable` (
                                 `service_id` int(11) NOT NULL,
                                 `local_admin_id` int(11) NOT NULL,
                                 `employee_id` int(11) NOT NULL,
                                 `day_id` int(1) NOT NULL COMMENT '1-> Monday; 7->Sunday',
                                 `time_from` time NOT NULL,
                                 `time_to` time NOT NULL,
                                 `main_id` int(25) NOT NULL
                                 ) ENGINE=HEAP DEFAULT CHARSET=latin1;


   curloop: loop
     
   fetch cur_biz into
           my_biz_hours_id,
           my_service_id,
           my_local_admin_id,
           my_employee_id,
           my_day_id,
           my_time_from,
           my_time_to,
           my_continuation_id ;
     
   if finished then
       close cur_biz;
       set finished = false;
       leave curloop;
   end if;
   SET final_day_id = my_day_id;


BLOCK2: BEGIN
       DECLARE inner_done boolean;
       DECLARE  no_of_row INT DEFAULT 0;
       DECLARE  inner_time_from, inner_time_to TIME;
       DECLARE innerCursor CURSOR FOR
  
   SELECT
       ADDTIME(time_from,time_zone) time_from,
       ADDTIME(time_to,time_zone) time_to
   FROM
       app_biz_hours
   WHERE
       continuation_id  = my_biz_hours_id;

       
DECLARE CONTINUE HANDLER FOR NOT FOUND SET inner_done = TRUE;

OPEN innerCursor ;
   cur_inner_loop: LOOP
       FETCH FROM innerCursor INTO
                   inner_time_from,
                   inner_time_to ;  
       
       IF inner_done THEN
           CLOSE innerCursor ;
           LEAVE cur_inner_loop;
       END IF;
           SET  no_of_row = no_of_row +1;
   END LOOP cur_inner_loop;


   IF no_of_row =0 THEN
         SET final_time_from = my_time_from;
         SET final_time_to     =ADDTIME(CAST('00:00:01' as TIME),my_time_to);
    ELSE
         SET final_time_from = my_time_from;
         SET final_time_to     = ADDTIME(CAST('00:00:01' as TIME),inner_time_to);
    END IF;


   SET first_value = SUBSTRING_INDEX(final_time_from, ':', 1 );
   IF first_value < 0 THEN
       SET final_time_from  =  ADDTIME(CAST('24:00:00' as TIME),final_time_from);
       IF final_day_id =1 THEN
           SET  final_day_id = 7;
       ELSE
           SET final_day_id  = final_day_id -1;
       END IF;
   ELSEIF first_value > 24 THEN
       SET final_time_from  =  SUBTIME(final_time_from, CAST('24:00:00' as TIME));  
       IF final_day_id =7 THEN
           SET  final_day_id =1;
       ELSE
           SET final_day_id  = final_day_id +1;
       END IF;
   END IF;

   

   SET first_value = SUBSTRING_INDEX(final_time_to, ':', 1 );
   IF first_value < 0 THEN
       SET final_time_to  =  ADDTIME(CAST('24:00:00' as TIME),final_time_to);              
   ELSEIF first_value  > 24 THEN
       SET final_time_to  =  SUBTIME(final_time_to, CAST('24:00:00' as TIME));
   END IF;


   INSERT INTO
       app_biz_hours_temptable (
                   service_id,
                   local_admin_id,
                   employee_id,
                   day_id,
                   time_from,
                   time_to,
                   main_id)
   values
       (my_service_id,
       my_local_admin_id,
       my_employee_id,
       final_day_id ,
       final_time_from,
       final_time_to,
       my_biz_hours_id);

END BLOCK2;
      
end loop curloop;

   SET @sql_stmt = 'SELECT ';
   IF in_start_data ='' THEN
       SET  @sql_stmt = CONCAT(@sql_stmt,' * ');
   ELSE
       SET  @sql_stmt = CONCAT(@sql_stmt,' ',in_start_data ,' ');
   END IF;
   
   SET @sql_stmt = CONCAT(@sql_stmt,' FROM app_biz_hours_temptable WHERE 1=1 ');
   IF in_string_data !='' THEN
       SET @sql_stmt =  CONCAT(@sql_stmt,' ',in_string_data);
   END IF;
   PREPARE statementExecute FROM @sql_stmt;
   EXECUTE statementExecute;

   DROP TEMPORARY TABLE IF EXISTS app_biz_hours_temptable;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`bookient`@`localhost` PROCEDURE `delete_row_app_temp_booking`()
BEGIN

   DELETE
   FROM app_temp_booking
   WHERE  TIMESTAMPDIFF(MINUTE, `time`, NOW()) < 1 ;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`bookient`@`localhost` PROCEDURE `getWorkTime`(
	IN locl_admin INT,
	IN time_zone varchar(15),
	IN in_string_data varchar(255),
	IN in_start_data varchar(255)
)
BEGIN
    DECLARE my_service_duration_min,my_biz_hours_id,my_service_id,my_local_admin_id, my_employee_id, my_day_id, my_continuation_id INT;
    DECLARE my_time_from, my_time_to TIME;
    DECLARE final_time_from, final_time_to TIME;
    DECLARE final_day_id INT;
    DECLARE finished boolean;
    DECLARE cnt INT DEFAULT 0;
    DECLARE first_value int;

     
   DECLARE cur_biz CURSOR FOR

   SELECT  
       bh.biz_hours_id,
       ser.service_duration_min,
       bh.service_id,
       bh.local_admin_id,
       bh.employee_id,
       bh.day_id,
       ADDTIME(time_from,time_zone) time_from,
       ADDTIME(time_to,time_zone) time_to ,
       bh.continuation_id
   FROM
       app_biz_hours AS bh, app_service AS ser
   WHERE
       bh.continuation_id=0
   AND
       bh.local_admin_id=locl_admin
   AND
       ser.service_id=bh.service_id
   ORDER BY
       bh.biz_hours_id;

   
   DECLARE continue handler for not found set finished = true;
OPEN cur_biz;

DROP TEMPORARY TABLE IF EXISTS app_biz_hours_temptable;

CREATE TEMPORARY TABLE IF NOT EXISTS `app_biz_hours_temptable` (
                                 `service_id` int(11) NOT NULL,
                                 `service_duration_min` int(11) NOT NULL,
                                 `local_admin_id` int(11) NOT NULL,
                                 `employee_id` int(11) NOT NULL,
                                 `day_id` int(1) NOT NULL COMMENT '1-> Monday; 7->Sunday',
                                 `time_from` time NOT NULL,
                                 `time_to` time NOT NULL,
                                 `main_id` int(25) NOT NULL
                                 ) ENGINE=HEAP DEFAULT CHARSET=latin1;


   curloop: loop
     
   fetch cur_biz into
           my_biz_hours_id,
           my_service_duration_min,
           my_service_id,
           my_local_admin_id,
           my_employee_id,
           my_day_id,
           my_time_from,
           my_time_to,
           my_continuation_id ;
     
   if finished then
       close cur_biz;
       set finished = false;
       leave curloop;
   end if;
   SET final_day_id = my_day_id;


BLOCK2: BEGIN
       DECLARE inner_done boolean;
       DECLARE  no_of_row INT DEFAULT 0;
       DECLARE  inner_time_from, inner_time_to TIME;
       DECLARE innerCursor CURSOR FOR
  
   SELECT
       ADDTIME(time_from,time_zone) time_from,
       ADDTIME(time_to,time_zone) time_to
   FROM
       app_biz_hours
   WHERE
       continuation_id  = my_biz_hours_id;

       
DECLARE CONTINUE HANDLER FOR NOT FOUND SET inner_done = TRUE;

OPEN innerCursor ;
   cur_inner_loop: LOOP
       FETCH FROM innerCursor INTO
                   inner_time_from,
                   inner_time_to ;  
       
       IF inner_done THEN
           CLOSE innerCursor ;
           LEAVE cur_inner_loop;
       END IF;
           SET  no_of_row = no_of_row +1;
   END LOOP cur_inner_loop;


   IF no_of_row =0 THEN
         SET final_time_from = my_time_from;
         SET final_time_to     =ADDTIME(CAST('00:00:01' as TIME),my_time_to);
    ELSE
         SET final_time_from = my_time_from;
         SET final_time_to     = ADDTIME(CAST('00:00:01' as TIME),inner_time_to);
    END IF;


   SET first_value = SUBSTRING_INDEX(final_time_from, ':', 1 );
   IF first_value < 0 THEN
       SET final_time_from  =  ADDTIME(CAST('24:00:00' as TIME),final_time_from);
       IF final_day_id =1 THEN
           SET  final_day_id = 7;
       ELSE
           SET final_day_id  = final_day_id -1;
       END IF;
   ELSEIF first_value > 24 THEN
       SET final_time_from  =  SUBTIME(final_time_from, CAST('24:00:00' as TIME));  
       IF final_day_id =7 THEN
           SET  final_day_id =1;
       ELSE
           SET final_day_id  = final_day_id +1;
       END IF;
   END IF;

   

   SET first_value = SUBSTRING_INDEX(final_time_to, ':', 1 );
   IF first_value < 0 THEN
       SET final_time_to  =  ADDTIME(CAST('24:00:00' as TIME),final_time_to);              
   ELSEIF first_value  > 24 THEN
       SET final_time_to  =  SUBTIME(final_time_to, CAST('24:00:00' as TIME));
   END IF;


   INSERT INTO
       app_biz_hours_temptable (
                   service_id,
                   service_duration_min,
                   local_admin_id,
                   employee_id,
                   day_id,
                   time_from,
                   time_to,
                   main_id)
   values
       (my_service_id,
       my_service_duration_min,
       my_local_admin_id,
       my_employee_id,
       final_day_id ,
       final_time_from,
       final_time_to,
       my_biz_hours_id);

END BLOCK2;
      
end loop curloop;

   SET @sql_stmt = 'SELECT ';
   IF in_start_data ='' THEN
       SET  @sql_stmt = CONCAT(@sql_stmt,' * ');
   ELSE
       SET  @sql_stmt = CONCAT(@sql_stmt,' ',in_start_data ,' ');
   END IF;
   
   SET @sql_stmt = CONCAT(@sql_stmt,' FROM app_biz_hours_temptable WHERE 1=1 ');
   IF in_string_data !='' THEN
       SET @sql_stmt =  CONCAT(@sql_stmt,' ',in_string_data);
   END IF;
   PREPARE statementExecute FROM @sql_stmt;
   EXECUTE statementExecute;

   DROP TEMPORARY TABLE IF EXISTS app_biz_hours_temptable;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`bookient`@`localhost` PROCEDURE `mainBizScheduleFrontend`(
	IN locl_admin INT,
	IN time_zone varchar(15),
	IN in_string_data varchar(255),
	IN in_start_data varchar(255)
)
BEGIN
    DECLARE my_biz_hours_id,my_service_id,my_local_admin_id, my_employee_id, my_day_id, my_continuation_id INT;
    DECLARE my_time_from, my_time_to TIME;
    DECLARE final_time_from, final_time_to TIME;
    DECLARE final_day_id INT;
    DECLARE finished boolean;
    DECLARE cnt INT DEFAULT 0;
    DECLARE first_value int;
    DECLARE duration int;
    DECLARE capacity int;

     
   DECLARE cur_biz CURSOR FOR

   SELECT  
       biz_hours_id,
       service_id,
       local_admin_id,
       employee_id,
       day_id,
       ADDTIME(time_from,time_zone) time_from,
       ADDTIME(time_to,time_zone) time_to ,
       continuation_id
   FROM
       app_biz_hours
   WHERE
       continuation_id=0
       AND
       local_admin_id=locl_admin
   ORDER BY
       biz_hours_id;

   
   DECLARE continue handler for not found set finished = true;
OPEN cur_biz;

DROP TEMPORARY TABLE IF EXISTS mainBizScheduleFrontendTmp;

CREATE TEMPORARY TABLE IF NOT EXISTS `mainBizScheduleFrontendTmp` (
                               `service_id` int(11) NOT NULL,
                               `local_admin_id` int(11) NOT NULL,
                               `employee_id` int(11) NOT NULL,
                               `day_id` int(1) NOT NULL COMMENT '1-> Monday; 7->Sunday',
                               `time_from` time NOT NULL,
                               `time_to` time NOT NULL,
                               `booking_capacity` int(11) NOT NULL,
                               `service_duration` int(11) NOT NULL,
                               `main_id` int(25) NOT NULL
                                 ) ENGINE=HEAP DEFAULT CHARSET=latin1;


   curloop: loop
     
   fetch cur_biz into
           my_biz_hours_id,
           my_service_id,
           my_local_admin_id,
           my_employee_id,
           my_day_id,
           my_time_from,
           my_time_to,
           my_continuation_id ;
     
   if finished then
       close cur_biz;
       set finished = false;
       leave curloop;
   end if;
   SET final_day_id = my_day_id;


BLOCK2: BEGIN
       DECLARE inner_done boolean;
       DECLARE  no_of_row INT DEFAULT 0;
       DECLARE  inner_time_from, inner_time_to TIME;
       DECLARE innerCursor CURSOR FOR
  
   SELECT
       ADDTIME(time_from,time_zone) time_from,
       ADDTIME(time_to,time_zone) time_to
   FROM
       app_biz_hours
   WHERE
       continuation_id  = my_biz_hours_id;

       
DECLARE CONTINUE HANDLER FOR NOT FOUND SET inner_done = TRUE;

OPEN innerCursor ;
   cur_inner_loop: LOOP
       FETCH FROM innerCursor INTO
                   inner_time_from,
                   inner_time_to ;  
       
       IF inner_done THEN
           CLOSE innerCursor ;
           LEAVE cur_inner_loop;
       END IF;
           SET  no_of_row = no_of_row +1;
   END LOOP cur_inner_loop;


   IF no_of_row =0 THEN
         SET final_time_from = my_time_from;
         SET final_time_to     =ADDTIME(CAST('00:00:01' as TIME),my_time_to);
    ELSE
         SET final_time_from = my_time_from;
         SET final_time_to     = ADDTIME(CAST('00:00:01' as TIME),inner_time_to);
    END IF;


   SET first_value = SUBSTRING_INDEX(final_time_from, ':', 1 );
   IF first_value < 0 THEN
       SET final_time_from  =  ADDTIME(CAST('24:00:00' as TIME),final_time_from);
       IF final_day_id =1 THEN
           SET  final_day_id = 7;
       ELSE
           SET final_day_id  = final_day_id -1;
       END IF;
   ELSEIF first_value > 24 THEN
       SET final_time_from  =  SUBTIME(final_time_from, CAST('24:00:00' as TIME));  
       IF final_day_id =7 THEN
           SET  final_day_id =1;
       ELSE
           SET final_day_id  = final_day_id +1;
       END IF;
   END IF;

   

   SET first_value = SUBSTRING_INDEX(final_time_to, ':', 1 );
   IF first_value < 0 THEN
       SET final_time_to  =  ADDTIME(CAST('24:00:00' as TIME),final_time_to);              
   ELSEIF first_value  > 24 THEN
       SET final_time_to  =  SUBTIME(final_time_to, CAST('24:00:00' as TIME));
   END IF;

BLOCK3: BEGIN

   DECLARE li_booking_capacity INT;
   DECLARE li_service_duration INT;
   DECLARE service_done boolean;
   DECLARE serviceCursor CURSOR FOR
   
   SELECT
       service_duration_min AS duration,
       service_capacity AS capacity
   FROM
       app_service
   WHERE
       service_id  = my_service_id;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET service_done = TRUE;

OPEN serviceCursor ;
   service_loop: LOOP
       FETCH FROM serviceCursor INTO
                   li_service_duration,
                   li_booking_capacity ;  
       
       IF service_done THEN
           CLOSE serviceCursor ;
           LEAVE service_loop;
       END IF;
   END LOOP service_loop;
   SET duration = li_service_duration;
   SET capacity = li_booking_capacity;

END BLOCK3;


   INSERT INTO
       mainBizScheduleFrontendTmp (
                   service_id,
                   local_admin_id,
                   employee_id,
                   day_id,
                   time_from,
                   time_to,
                   booking_capacity,
                   service_duration,
                   main_id)
   values
       (my_service_id,
       my_local_admin_id,
       my_employee_id,
       final_day_id ,
       final_time_from,
       final_time_to,
       capacity,
       duration,            
       my_biz_hours_id);

END BLOCK2;
      
end loop curloop;

   SET @sql_stmt = 'SELECT ';
   IF in_start_data ='' THEN
       SET  @sql_stmt = CONCAT(@sql_stmt,' * ');
   ELSE
       SET  @sql_stmt = CONCAT(@sql_stmt,' ',in_start_data ,' ');
   END IF;
   
   SET @sql_stmt = CONCAT(@sql_stmt,' FROM mainBizScheduleFrontendTmp WHERE 1=1 ');
   IF in_string_data !='' THEN
       SET @sql_stmt =  CONCAT(@sql_stmt,' ',in_string_data);
   END IF;
   PREPARE statementExecute FROM @sql_stmt;
   EXECUTE statementExecute;

   DROP TEMPORARY TABLE IF EXISTS mainBizScheduleFrontendTmp;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`bookient`@`localhost` PROCEDURE `pardco_appointment_no_show_graph`(
	IN from_date VARCHAR(255),
	IN to_date VARCHAR(255),
	IN local_admin_id VARCHAR(255)
)
BEGIN
       DECLARE info_date1 DATE;
       DECLARE info1 VARCHAR(250);
       DECLARE cnt1  INTEGER (11) UNSIGNED;
       DECLARE chkentry  INTEGER (11) UNSIGNED;
       
           DECLARE graphDone INT;
           DECLARE appointmentnoshowgraphcursor CURSOR FOR
        SELECT * FROM (
           (
           SELECT booking_date_time AS info_date, COUNT( * ) AS cnt, '1' AS info
           FROM app_booking
           WHERE booking_date_time >= from_date
           AND booking_date_time <= to_date
           AND local_admin_id = local_admin_id
           GROUP BY DATE_FORMAT( booking_date_time, '%Y-%m-%d' ) , info
           )
           UNION (
           SELECT no_show_date AS info_date, COUNT( * ) AS cnt, '0' AS info
           FROM app_booking_service
           WHERE no_show_date >= from_date
           AND no_show_date <= to_date
           AND local_admin_id = local_admin_id
           GROUP BY DATE_FORMAT( no_show_date, '%Y-%m-%d' ) , info
           )
           ) AS X ORDER BY info_date, info ASC;
       DECLARE CONTINUE HANDLER FOR NOT FOUND SET graphDone = 1;
       TRUNCATE TABLE appointment_no_show_graph;
       SET graphDone = 0;
       OPEN appointmentnoshowgraphcursor;
       graphIterator: LOOP
       FETCH appointmentnoshowgraphcursor INTO info_date1,cnt1,info1;
       IF 1 = graphDone THEN
           LEAVE graphIterator;
       END IF;
       BLOCK2: BEGIN
           
       SELECT COUNT(*) mycnt INTO chkentry FROM appointment_no_show_graph WHERE info_date = info_date1;
       IF(chkentry>0) THEN
            IF(info1=1)THEN
           UPDATE appointment_no_show_graph SET booking_cnt= cnt1 WHERE info_date = info_date1;
            ELSE
           UPDATE appointment_no_show_graph SET no_show_cnt= cnt1 WHERE info_date = info_date1;    
            END IF;    
       ELSE
           IF(info1=1)THEN
           INSERT INTO appointment_no_show_graph (info_date,booking_cnt) VALUES(info_date1,cnt1);
            ELSE
           INSERT INTO appointment_no_show_graph (info_date,no_show_cnt)  VALUES(info_date1,cnt1);    
            END IF;    
       END IF;    
           
       END BLOCK2;
       END LOOP graphIterator;
       CLOSE appointmentnoshowgraphcursor;
   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`bookient`@`localhost` PROCEDURE `pardco_graph`(
	IN from_date VARCHAR(255),
	IN to_date VARCHAR(255),
	IN local_admin_id VARCHAR(255)
)
BEGIN
           DECLARE info_date1 DATE;
       DECLARE info1 VARCHAR(250);
       DECLARE cnt1  INTEGER (11) UNSIGNED;
       DECLARE chkentry  INTEGER (11) UNSIGNED;
       
           DECLARE graphDone INT;
           DECLARE graphcursor CURSOR FOR
               SELECT * FROM (( SELECT booking_date_time AS info_date, COUNT( * ) AS cnt, '1' AS info
                   FROM app_booking
                   WHERE booking_date_time >= from_date
                   AND booking_date_time <= to_date
                   AND local_admin_id = local_admin_id
                   GROUP BY DATE_FORMAT( booking_date_time, '%Y-%m-%d' ) , info)
               UNION (
                   SELECT cancellation_date AS info_date, COUNT( * ) AS cnt, '0' AS info
                   FROM app_booking_service
                   WHERE cancellation_date >= from_date
                   AND cancellation_date <= to_date
                   AND local_admin_id = local_admin_id
                   GROUP BY DATE_FORMAT( cancellation_date, '%Y-%m-%d' ) , info
                     )
                   ) AS X  ORDER BY info_date, info;
                       
           DECLARE CONTINUE HANDLER FOR NOT FOUND SET graphDone = 1;
           TRUNCATE TABLE graph_temp;
           SET graphDone = 0;
           OPEN graphcursor;
           graphIterator: LOOP
               FETCH graphcursor INTO info_date1,cnt1,info1;
               IF 1 = graphDone THEN
                   LEAVE graphIterator;
               END IF;
       BLOCK2: BEGIN
           
       SELECT COUNT(*) mycnt INTO chkentry FROM graph_temp WHERE info_date = info_date1;
       IF(chkentry>0) THEN
            IF(info1=1)THEN
           UPDATE graph_temp SET booking_cnt= cnt1 WHERE info_date = info_date1;
            ELSE
               UPDATE graph_temp SET cancel_cnt= cnt1 WHERE info_date = info_date1;    
            END IF;    
       ELSE
           IF(info1=1)THEN
           INSERT INTO graph_temp (info_date,booking_cnt) VALUES(info_date1,cnt1);
            ELSE
               INSERT INTO graph_temp (info_date,cancel_cnt)  VALUES(info_date1,cnt1);    
            END IF;    
       END IF;    
           
       END BLOCK2;
           END LOOP graphIterator;
           CLOSE graphcursor;
                         
         
       END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`bookient`@`localhost` PROCEDURE `pardco_theme`(
	IN user_id INT(5),
	OUT return_data VARCHAR(255)
)
BEGIN
   DECLARE themeData VARCHAR(255);
   DECLARE themeType VARCHAR(255);
   DECLARE themeCursor CURSOR FOR
   SELECT `theme` FROM `app_local_admin_gen_setting` WHERE `local_admin_id` = 6;                
   DECLARE CONTINUE HANDLER FOR NOT FOUND SET themeData = 1;
   SET themeData = 0;
   OPEN themeCursor;
   
   FETCH themeCursor INTO themeType;
        IF themeType = 'CCS' THEN
       BEGIN
       DECLARE themedetailsCursor CURSOR FOR
           SELECT * FROM `app_custom_color_scheme` WHERE `local_admin_id` = 0 AND `theme_name` = themeType;
       END;
        ELSE
       BEGIN
       DECLARE themedetailsCursor CURSOR FOR
           SELECT * FROM `app_custom_color_scheme` WHERE `local_admin_id` = user_id AND `theme_name` = themeType;
       END;
        END IF;
   
   CLOSE themeCursor;
           
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`bookient`@`localhost` PROCEDURE `reviewDetailsReport`(
	IN `locl_admin` int,
	IN `time_zone` varchar(15),
	IN `in_string_data` varchar(255),
	IN `in_start_data` varchar(255)
)
BEGIN

   DECLARE li_booking_id,
       li_customer_id INT;
   DECLARE lt_booking_date_time DATETIME;
   DECLARE ls_total_tax varchar(100);
   DECLARE ls_discount_amnt varchar(100);
   DECLARE ls_grand_total varchar(100);
   DECLARE ls_prepayment_amount  varchar(100);
   DECLARE ls_grand_sub_cost varchar(100);
   DECLARE ls_discount_coupon_dtls varchar(100);
   DECLARE ls_tax_dtls_arr varchar(100);
   DECLARE ls_prepayment_details varchar(100);
   DECLARE ls_comment varchar(255);
   DECLARE first_cursor boolean;
   DECLARE temp_booking_id INT;
       DECLARE ls_currency_id INT;
       
   
   DECLARE bookingTab_cursor CURSOR FOR


           SELECT    
               booking_id,
               customer_id,
                               currency_id,
               ADDTIME(booking_date_time,time_zone) booking_date_time,
               booking_sub_total,
               booking_disc_amount,
               booking_disc_coupon_details,
               booking_total_tax,
               booking_tax_dtls_arr,
               booking_grnd_total,
               booking_prepayment_amount,
               booking_prepayment_details,
               booking_comment
           FROM    
               app_booking
           WHERE    
               booking_is_deleted = '0'
               AND
               local_admin_id = locl_admin
           ORDER BY
               booking_id;

   DECLARE continue handler for not found set first_cursor = true;
OPEN bookingTab_cursor;

DROP TEMPORARY TABLE IF EXISTS app_temp_booking;

CREATE TEMPORARY TABLE IF NOT EXISTS `app_temp_booking_review` (
                       `booking_id` int(11) NOT NULL,
                       `customer_id` int(11) NOT NULL,
                                               `currency_id` int(11) NOT NULL,
                       `booking_date_time` DATETIME NOT NULL,
                       `booking_sub_total` varchar(100) NOT NULL,
                       `booking_disc_amount` varchar(100) NOT NULL,
                       `booking_disc_coupon_details` varchar(255) NOT NULL,
                       `booking_total_tax` varchar(100) NOT NULL,
                       `booking_tax_dtls_arr` varchar(255) NOT NULL,
                       `booking_grnd_total` varchar(100) NOT NULL,
                       `booking_prepayment_amount` varchar(100) NOT NULL,
                       `booking_prepayment_details` varchar(255) NOT NULL,
                       `booking_comment` varchar(255) NOT NULL,
                       `srvDtls_id` int(11) NOT NULL,
                       `srvDtls_service_id` int(11) NOT NULL,
                       `srvDtls_service_name` varchar(255) NOT NULL,
                       `srvDtls_service_cost` varchar(100) NOT NULL,
                       `srvDtls_service_duration` int(11) NOT NULL,
                       `srvDtls_service_duration_unit` varchar(10) NOT NULL,
                       `srvDtls_service_start` DATETIME NOT NULL,
                       `srvDtls_service_end` DATETIME NOT NULL,
                       `srvDtls_employee_id` int(11) NOT NULL,
                       `srvDtls_employee_name` varchar(255) NOT NULL,
                       `srvDtls_booking_status` varchar(255) NOT NULL,
                       `srvDtls_status_date` DATETIME NOT NULL,
                       `srvDtls_service_quantity` int(11) NOT NULL,
                       `srvDtls_service_description` varchar(255) NOT NULL
                             ) ENGINE=HEAP DEFAULT CHARSET=latin1;

   curloop: loop

       fetch bookingTab_cursor into
                   li_booking_id,
                   li_customer_id,
                                       ls_currency_id,
                   lt_booking_date_time,
                   ls_grand_sub_cost,
                   ls_discount_amnt,
                   ls_discount_coupon_dtls,
                   ls_total_tax,
                   ls_tax_dtls_arr,
                   ls_grand_total,
                   ls_prepayment_amount,
                   ls_prepayment_details,
                   ls_comment ;

       if first_cursor then
           close bookingTab_cursor;
           set first_cursor = false;
           leave curloop;
       end if;
       SET temp_booking_id = li_booking_id ;
BLOCK2: BEGIN

   DECLARE srvLi_srvDtls_id,
       srvLi_srvDtls_service_id INT;
       DECLARE srvLs_srvDtls_service_name varchar(255);
   DECLARE srvLs_srvDtls_service_cost varchar(255);
   DECLARE srvLi_srvDtls_service_duration INT;
   DECLARE srvLs_srvDtls_service_duration_unit varchar(100);
   DECLARE srvLt_srvDtls_service_start DATETIME;
   DECLARE srvLt_srvDtls_service_end DATETIME;
   DECLARE srvLi_srvDtls_continuation_id INT;
   DECLARE srvLi_srvDtls_employee_id INT;
   DECLARE srvLs_srvDtls_employee_name varchar(100);
   DECLARE srvLi_srvDtls_booking_status INT;
   DECLARE srvLt_srvDtls_status_date DATETIME;
   DECLARE srvLi_srvDtls_service_quantity INT;
   DECLARE srvLi_rescheduled_child_id INT;
   DECLARE srvLs_srvDtls_service_description varchar(255);
   DECLARE inner_done boolean;
       DECLARE innerCursor CURSOR FOR


           SELECT
               srvDtls_id,
               srvDtls_service_id,
               srvDtls_service_name,
               srvDtls_service_cost,
               srvDtls_service_duration,
               srvDtls_service_duration_unit,
               ADDTIME(srvDtls_service_start,time_zone) srvDtls_service_start,
               ADDTIME(srvDtls_service_end,time_zone) srvDtls_service_end,
               srvDtls_employee_id,
               srvDtls_employee_name,
               srvDtls_booking_status,
               ADDTIME(srvDtls_status_date,time_zone) srvDtls_status_date,
               srvDtls_service_quantity,
               srvDtls_service_description
           FROM
               app_booking_service_details
           WHERE
               srvDtls_booking_id  = temp_booking_id
               AND
               srvDtls_rescheduled_child_id = 0
           ORDER BY
               srvDtls_id;


DECLARE CONTINUE HANDLER FOR NOT FOUND SET inner_done = TRUE;
OPEN innerCursor ;
   cur_inner_loop: LOOP

       FETCH FROM innerCursor INTO
                   srvLi_srvDtls_id,
                   srvLi_srvDtls_service_id,
                   srvLs_srvDtls_service_name,
                   srvLs_srvDtls_service_cost,
                   srvLi_srvDtls_service_duration,
                   srvLs_srvDtls_service_duration_unit,
                   srvLt_srvDtls_service_start,
                   srvLt_srvDtls_service_end,
                   srvLi_srvDtls_employee_id,
                   srvLs_srvDtls_employee_name,
                   srvLi_srvDtls_booking_status,
                   srvLt_srvDtls_status_date,
                   srvLi_srvDtls_service_quantity,
                   srvLs_srvDtls_service_description ;  

       
       IF inner_done THEN
           CLOSE innerCursor ;
           LEAVE cur_inner_loop;
       END IF;

   INSERT INTO
       app_temp_booking_review (
               booking_id,
               customer_id,
                               currency_id,
               booking_date_time,
               booking_sub_total,
               booking_disc_amount,
               booking_disc_coupon_details,
               booking_total_tax,
               booking_tax_dtls_arr,
               booking_grnd_total,
               booking_prepayment_amount,
               booking_prepayment_details,
               booking_comment,
               srvDtls_id,
               srvDtls_service_id,
               srvDtls_service_name,
               srvDtls_service_cost,
               srvDtls_service_duration,
               srvDtls_service_duration_unit,
               srvDtls_service_start,
               srvDtls_service_end,
               srvDtls_employee_id,
               srvDtls_employee_name,
               srvDtls_booking_status,
               srvDtls_status_date,
               srvDtls_service_quantity,
               srvDtls_service_description
               )values(
               li_booking_id,
               li_customer_id,
               ls_currency_id,
               lt_booking_date_time,
               ls_grand_sub_cost,
               ls_discount_amnt,
               ls_discount_coupon_dtls,
               ls_total_tax,
               ls_tax_dtls_arr,
               ls_grand_total,
               ls_prepayment_amount,
               ls_prepayment_details,
               ls_comment,
               srvLi_srvDtls_id,
               srvLi_srvDtls_service_id,
               srvLs_srvDtls_service_name,
               srvLs_srvDtls_service_cost,
               srvLi_srvDtls_service_duration,
               srvLs_srvDtls_service_duration_unit,
               srvLt_srvDtls_service_start,
               srvLt_srvDtls_service_end,
               srvLi_srvDtls_employee_id,
               srvLs_srvDtls_employee_name,
               srvLi_srvDtls_booking_status,
               srvLt_srvDtls_status_date,
               srvLi_srvDtls_service_quantity,
               srvLs_srvDtls_service_description        
               );

END LOOP cur_inner_loop;
END BLOCK2;
   end loop curloop;

   SET @sql_stmt = 'SELECT ';
   IF in_start_data ='' THEN
       SET  @sql_stmt = CONCAT(@sql_stmt,' * ');
   ELSE
       SET  @sql_stmt = CONCAT(@sql_stmt,' ',in_start_data ,' ');
   END IF;
   
   SET @sql_stmt = CONCAT(@sql_stmt,' FROM app_temp_booking_review right join app_review ON app_temp_booking_review.srvDtls_id = app_review.srvDtls_id LEFT JOIN vw_customerdetails  ON app_temp_booking_review.customer_id =vw_customerdetails.user_id WHERE 1=1 ');
   
   
   IF in_string_data !='' THEN
       SET @sql_stmt =  CONCAT(@sql_stmt,' ',in_string_data);
   END IF;
   PREPARE statementExecute FROM @sql_stmt;
   EXECUTE statementExecute;

   DROP TEMPORARY TABLE IF EXISTS app_temp_booking_review;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`bookient`@`localhost` PROCEDURE `sp_booking`(
	IN locl_admin INT,
	IN time_zone varchar(15),
	IN in_string_data varchar(255),
	IN in_start_data varchar(255)
)
BEGIN

   DECLARE li_booking_id,
       li_customer_id INT;
   DECLARE lt_booking_date_time DATETIME;
   DECLARE ls_total_tax varchar(100);
   DECLARE ls_discount_amnt varchar(100);
   DECLARE ls_grand_total varchar(100);
   DECLARE ls_prepayment_amount  varchar(100);
   DECLARE ls_grand_sub_cost varchar(100);
   DECLARE ls_discount_coupon_dtls varchar(100);
   DECLARE ls_tax_dtls_arr varchar(100);
   DECLARE ls_prepayment_details varchar(100);
   DECLARE ls_comment varchar(255);
   DECLARE first_cursor boolean;
   DECLARE temp_booking_id INT;
       DECLARE ls_currency_id INT;
       
   
   DECLARE bookingTab_cursor CURSOR FOR


           SELECT    
               booking_id,
               customer_id,
                               currency_id,
               ADDTIME(booking_date_time,time_zone) booking_date_time,
               booking_sub_total,
               booking_disc_amount,
               booking_disc_coupon_details,
               booking_total_tax,
               booking_tax_dtls_arr,
               booking_grnd_total,
               booking_prepayment_amount,
               booking_prepayment_details,
               booking_comment
           FROM    
               app_booking
           WHERE    
               booking_is_deleted = '0'
               AND
               local_admin_id = locl_admin
           ORDER BY
               booking_id;

   DECLARE continue handler for not found set first_cursor = true;
OPEN bookingTab_cursor;

DROP TEMPORARY TABLE IF EXISTS app_temp_booking;

CREATE TEMPORARY TABLE IF NOT EXISTS `app_temp_booking` (
                       `booking_id` int(11) NOT NULL,
                       `customer_id` int(11) NOT NULL,
                                               `currency_id` int(11) NOT NULL,
                       `booking_date_time` DATETIME NOT NULL,
                       `booking_sub_total` varchar(100) NOT NULL,
                       `booking_disc_amount` varchar(100) NOT NULL,
                       `booking_disc_coupon_details` varchar(255) NOT NULL,
                       `booking_total_tax` varchar(100) NOT NULL,
                       `booking_tax_dtls_arr` varchar(255) NOT NULL,
                       `booking_grnd_total` varchar(100) NOT NULL,
                       `booking_prepayment_amount` varchar(100) NOT NULL,
                       `booking_prepayment_details` varchar(255) NOT NULL,
                       `booking_comment` varchar(255) NOT NULL,
                       `srvDtls_id` int(11) NOT NULL,
                       `srvDtls_service_id` int(11) NOT NULL,
                       `srvDtls_service_name` varchar(255) NOT NULL,
                       `srvDtls_service_cost` varchar(100) NOT NULL,
                       `srvDtls_service_duration` int(11) NOT NULL,
                       `srvDtls_service_duration_unit` varchar(10) NOT NULL,
                       `srvDtls_service_start` DATETIME NOT NULL,
                       `srvDtls_service_end` DATETIME NOT NULL,
                       `srvDtls_employee_id` int(11) NOT NULL,
                       `srvDtls_employee_name` varchar(255) NOT NULL,
                       `srvDtls_booking_status` varchar(255) NOT NULL,
                       `srvDtls_status_date` DATETIME NOT NULL,
                       `srvDtls_service_quantity` int(11) NOT NULL,
                       `srvDtls_service_description` varchar(255) NOT NULL
                             ) ENGINE=HEAP DEFAULT CHARSET=latin1;

   curloop: loop

       fetch bookingTab_cursor into
                   li_booking_id,
                   li_customer_id,
                                       ls_currency_id,
                   lt_booking_date_time,
                   ls_grand_sub_cost,
                   ls_discount_amnt,
                   ls_discount_coupon_dtls,
                   ls_total_tax,
                   ls_tax_dtls_arr,
                   ls_grand_total,
                   ls_prepayment_amount,
                   ls_prepayment_details,
                   ls_comment ;

       if first_cursor then
           close bookingTab_cursor;
           set first_cursor = false;
           leave curloop;
       end if;
       SET temp_booking_id = li_booking_id ;
BLOCK2: BEGIN

   DECLARE srvLi_srvDtls_id,
       srvLi_srvDtls_service_id INT;
       DECLARE srvLs_srvDtls_service_name varchar(255);
   DECLARE srvLs_srvDtls_service_cost varchar(255);
   DECLARE srvLi_srvDtls_service_duration INT;
   DECLARE srvLs_srvDtls_service_duration_unit varchar(100);
   DECLARE srvLt_srvDtls_service_start DATETIME;
   DECLARE srvLt_srvDtls_service_end DATETIME;
   DECLARE srvLi_srvDtls_continuation_id INT;
   DECLARE srvLi_srvDtls_employee_id INT;
   DECLARE srvLs_srvDtls_employee_name varchar(100);
   DECLARE srvLi_srvDtls_booking_status INT;
   DECLARE srvLt_srvDtls_status_date DATETIME;
   DECLARE srvLi_srvDtls_service_quantity INT;
   DECLARE srvLi_rescheduled_child_id INT;
   DECLARE srvLs_srvDtls_service_description varchar(255);
   DECLARE inner_done boolean;
       DECLARE innerCursor CURSOR FOR


           SELECT
               srvDtls_id,
               srvDtls_service_id,
               srvDtls_service_name,
               srvDtls_service_cost,
               srvDtls_service_duration,
               srvDtls_service_duration_unit,
               ADDTIME(srvDtls_service_start,time_zone) srvDtls_service_start,
               ADDTIME(srvDtls_service_end,time_zone) srvDtls_service_end,
               srvDtls_employee_id,
               srvDtls_employee_name,
               srvDtls_booking_status,
               ADDTIME(srvDtls_status_date,time_zone) srvDtls_status_date,
               srvDtls_service_quantity,
               srvDtls_service_description
           FROM
               app_booking_service_details
           WHERE
               srvDtls_booking_id  = temp_booking_id
               AND
               srvDtls_rescheduled_child_id = 0
           ORDER BY
               srvDtls_id;


DECLARE CONTINUE HANDLER FOR NOT FOUND SET inner_done = TRUE;
OPEN innerCursor ;
   cur_inner_loop: LOOP

       FETCH FROM innerCursor INTO
                   srvLi_srvDtls_id,
                   srvLi_srvDtls_service_id,
                   srvLs_srvDtls_service_name,
                   srvLs_srvDtls_service_cost,
                   srvLi_srvDtls_service_duration,
                   srvLs_srvDtls_service_duration_unit,
                   srvLt_srvDtls_service_start,
                   srvLt_srvDtls_service_end,
                   srvLi_srvDtls_employee_id,
                   srvLs_srvDtls_employee_name,
                   srvLi_srvDtls_booking_status,
                   srvLt_srvDtls_status_date,
                   srvLi_srvDtls_service_quantity,
                   srvLs_srvDtls_service_description ;  

       
       IF inner_done THEN
           CLOSE innerCursor ;
           LEAVE cur_inner_loop;
       END IF;

   INSERT INTO
       app_temp_booking (
               booking_id,
               customer_id,
                               currency_id,
               booking_date_time,
               booking_sub_total,
               booking_disc_amount,
               booking_disc_coupon_details,
               booking_total_tax,
               booking_tax_dtls_arr,
               booking_grnd_total,
               booking_prepayment_amount,
               booking_prepayment_details,
               booking_comment,
               srvDtls_id,
               srvDtls_service_id,
               srvDtls_service_name,
               srvDtls_service_cost,
               srvDtls_service_duration,
               srvDtls_service_duration_unit,
               srvDtls_service_start,
               srvDtls_service_end,
               srvDtls_employee_id,
               srvDtls_employee_name,
               srvDtls_booking_status,
               srvDtls_status_date,
               srvDtls_service_quantity,
               srvDtls_service_description
               )values(
               li_booking_id,
               li_customer_id,
               ls_currency_id,
               lt_booking_date_time,
               ls_grand_sub_cost,
               ls_discount_amnt,
               ls_discount_coupon_dtls,
               ls_total_tax,
               ls_tax_dtls_arr,
               ls_grand_total,
               ls_prepayment_amount,
               ls_prepayment_details,
               ls_comment,
               srvLi_srvDtls_id,
               srvLi_srvDtls_service_id,
               srvLs_srvDtls_service_name,
               srvLs_srvDtls_service_cost,
               srvLi_srvDtls_service_duration,
               srvLs_srvDtls_service_duration_unit,
               srvLt_srvDtls_service_start,
               srvLt_srvDtls_service_end,
               srvLi_srvDtls_employee_id,
               srvLs_srvDtls_employee_name,
               srvLi_srvDtls_booking_status,
               srvLt_srvDtls_status_date,
               srvLi_srvDtls_service_quantity,
               srvLs_srvDtls_service_description        
               );

END LOOP cur_inner_loop;
END BLOCK2;
   end loop curloop;

   SET @sql_stmt = 'SELECT ';
   IF in_start_data ='' THEN
       SET  @sql_stmt = CONCAT(@sql_stmt,' * ');
   ELSE
       SET  @sql_stmt = CONCAT(@sql_stmt,' ',in_start_data ,' ');
   END IF;
   
   SET @sql_stmt = CONCAT(@sql_stmt,' FROM app_temp_booking WHERE 1=1 ');
   IF in_string_data !='' THEN
       SET @sql_stmt =  CONCAT(@sql_stmt,' ',in_string_data);
   END IF;
   PREPARE statementExecute FROM @sql_stmt;
   EXECUTE statementExecute;

   DROP TEMPORARY TABLE IF EXISTS app_temp_booking;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`bookient`@`localhost` PROCEDURE `sp_report_booking_details`(
	IN locl_admin INT,
	IN time_zone varchar(15),
	IN in_string_data varchar(255),
	IN in_start_data varchar(255)
)
BEGIN
   DECLARE li_booking_id,
       li_customer_id INT;
   DECLARE lt_booking_date_time DATETIME;
   DECLARE ls_total_tax varchar(100);
   DECLARE ls_discount_amnt varchar(100);
   DECLARE ls_grand_total varchar(100);
   DECLARE ls_prepayment_amount  varchar(100);
   DECLARE ls_grand_sub_cost varchar(100);
   DECLARE ls_discount_coupon_dtls varchar(100);
   DECLARE ls_tax_dtls_arr varchar(100);
   DECLARE ls_prepayment_details varchar(100);
   DECLARE ls_comment varchar(255);
   DECLARE ls_added_by INT;
   DECLARE ls_booking_from INT;
   DECLARE first_cursor boolean;
   DECLARE temp_booking_id INT;
   DECLARE ls_currency_id INT;
           
   DECLARE bookingTab_cursor CURSOR FOR
           SELECT    
               booking_id,
               customer_id,
               currency_id,
               ADDTIME(booking_date_time,time_zone) booking_date_time,
               booking_sub_total,
               booking_disc_amount,
               booking_disc_coupon_details,
               booking_total_tax,
               booking_tax_dtls_arr,
               booking_grnd_total,
               booking_prepayment_amount,
               booking_prepayment_details,
               booking_comment,
               added_by,
               booking_from
           FROM    
               app_booking
           WHERE    
               booking_is_deleted = '0'
               AND
               local_admin_id = locl_admin
           ORDER BY
               booking_id;
   DECLARE continue handler for not found set first_cursor = true;
OPEN bookingTab_cursor;
DROP TEMPORARY TABLE IF EXISTS app_temp_booking_repo;

CREATE TEMPORARY TABLE IF NOT EXISTS `app_temp_booking_repo` (
                       `booking_id` int(11) NOT NULL,
                       `customer_id` int(11) NOT NULL,
                       `currency_id` int(11) NOT NULL,
                       `booking_date_time` DATETIME NOT NULL,
                       `booking_sub_total` varchar(100) NOT NULL,
                       `booking_disc_amount` varchar(100) NOT NULL,
                       `booking_disc_coupon_details` varchar(255) NOT NULL,
                       `booking_total_tax` varchar(100) NOT NULL,
                       `booking_tax_dtls_arr` varchar(255) NOT NULL,
                       `booking_grnd_total` varchar(100) NOT NULL,
                       `booking_prepayment_amount` varchar(100) NOT NULL,
                       `booking_prepayment_details` varchar(255) NOT NULL,
                       `booking_comment` varchar(255) NOT NULL,
                       `added_by` int(11) NOT NULL,
                       `booking_from` int(11) NOT NULL,
                       `srvDtls_id` int(11) NOT NULL,
                       `srvDtls_service_id` int(11) NOT NULL,
                       `srvDtls_service_name` varchar(255) NOT NULL,
                       `srvDtls_service_cost` varchar(100) NOT NULL,
                       `srvDtls_service_duration` int(11) NOT NULL,
                       `srvDtls_service_duration_unit` varchar(10) NOT NULL,
                       `srvDtls_service_start` DATETIME NOT NULL,
                       `srvDtls_service_end` DATETIME NOT NULL,
                       `srvDtls_employee_id` int(11) NOT NULL,
                       `srvDtls_employee_name` varchar(255) NOT NULL,
                       `srvDtls_booking_status` varchar(255) NOT NULL,
                       `srvDtls_status_date` DATETIME NOT NULL,
                       `srvDtls_service_quantity` int(11) NOT NULL,
                       `srvDtls_service_description` varchar(255) NOT NULL
                             ) ENGINE=HEAP DEFAULT CHARSET=latin1;
   curloop: loop
       fetch bookingTab_cursor into
                   li_booking_id,
                   li_customer_id,
                   ls_currency_id,
                   lt_booking_date_time,
                   ls_grand_sub_cost,
                   ls_discount_amnt,
                   ls_discount_coupon_dtls,
                   ls_total_tax,
                   ls_tax_dtls_arr,
                   ls_grand_total,
                   ls_prepayment_amount,
                   ls_prepayment_details,
                   ls_comment,
                   ls_added_by,
                   ls_booking_from ;
       if first_cursor then
           close bookingTab_cursor;
           set first_cursor = false;
           leave curloop;
       end if;
       SET temp_booking_id = li_booking_id ;
BLOCK2: BEGIN
   DECLARE srvLi_srvDtls_id,
       srvLi_srvDtls_service_id INT;
   DECLARE srvLs_srvDtls_service_name varchar(255);
   DECLARE srvLs_srvDtls_service_cost varchar(255);
   DECLARE srvLi_srvDtls_service_duration INT;
   DECLARE srvLs_srvDtls_service_duration_unit varchar(100);
   DECLARE srvLt_srvDtls_service_start DATETIME;
   DECLARE srvLt_srvDtls_service_end DATETIME;
   DECLARE srvLi_srvDtls_continuation_id INT;
   DECLARE srvLi_srvDtls_employee_id INT;
   DECLARE srvLs_srvDtls_employee_name varchar(100);
   DECLARE srvLi_srvDtls_booking_status INT;
   DECLARE srvLt_srvDtls_status_date DATETIME;
   DECLARE srvLi_srvDtls_service_quantity INT;
   DECLARE srvLi_rescheduled_child_id INT;
   DECLARE srvLs_srvDtls_service_description varchar(255);
   DECLARE inner_done boolean;
       DECLARE innerCursor CURSOR FOR
           SELECT
               srvDtls_id,
               srvDtls_service_id,
               srvDtls_service_name,
               srvDtls_service_cost,
               srvDtls_service_duration,
               srvDtls_service_duration_unit,
               ADDTIME(srvDtls_service_start,time_zone) srvDtls_service_start,
               ADDTIME(srvDtls_service_end,time_zone) srvDtls_service_end,
               srvDtls_employee_id,
               srvDtls_employee_name,
               srvDtls_booking_status,
               ADDTIME(srvDtls_status_date,time_zone) srvDtls_status_date,
               srvDtls_service_quantity,
               srvDtls_service_description
           FROM
               app_booking_service_details
           WHERE
               srvDtls_booking_id  = temp_booking_id
               AND
               srvDtls_rescheduled_child_id = 0
           ORDER BY
               srvDtls_id;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET inner_done = TRUE;
OPEN innerCursor ;
   cur_inner_loop: LOOP
       FETCH FROM innerCursor INTO
                   srvLi_srvDtls_id,
                   srvLi_srvDtls_service_id,
                   srvLs_srvDtls_service_name,
                   srvLs_srvDtls_service_cost,
                   srvLi_srvDtls_service_duration,
                   srvLs_srvDtls_service_duration_unit,
                   srvLt_srvDtls_service_start,
                   srvLt_srvDtls_service_end,
                   srvLi_srvDtls_employee_id,
                   srvLs_srvDtls_employee_name,
                   srvLi_srvDtls_booking_status,
                   srvLt_srvDtls_status_date,
                   srvLi_srvDtls_service_quantity,
                   srvLs_srvDtls_service_description ;  
       
       IF inner_done THEN
           CLOSE innerCursor ;
           LEAVE cur_inner_loop;
       END IF;
   INSERT INTO
       app_temp_booking_repo (
               booking_id,
               customer_id,
               currency_id,
               booking_date_time,
               booking_sub_total,
               booking_disc_amount,
               booking_disc_coupon_details,
               booking_total_tax,
               booking_tax_dtls_arr,
               booking_grnd_total,
               booking_prepayment_amount,
               booking_prepayment_details,
               booking_comment,
               added_by,
               booking_from,
               srvDtls_id,
               srvDtls_service_id,
               srvDtls_service_name,
               srvDtls_service_cost,
               srvDtls_service_duration,
               srvDtls_service_duration_unit,
               srvDtls_service_start,
               srvDtls_service_end,
               srvDtls_employee_id,
               srvDtls_employee_name,
               srvDtls_booking_status,
               srvDtls_status_date,
               srvDtls_service_quantity,
               srvDtls_service_description
               )values(
               li_booking_id,
               li_customer_id,
               ls_currency_id,
               lt_booking_date_time,
               ls_grand_sub_cost,
               ls_discount_amnt,
               ls_discount_coupon_dtls,
               ls_total_tax,
               ls_tax_dtls_arr,
               ls_grand_total,
               ls_prepayment_amount,
               ls_prepayment_details,
               ls_comment,
               ls_added_by,
               ls_booking_from,
               srvLi_srvDtls_id,
               srvLi_srvDtls_service_id,
               srvLs_srvDtls_service_name,
               srvLs_srvDtls_service_cost,
               srvLi_srvDtls_service_duration,
               srvLs_srvDtls_service_duration_unit,
               srvLt_srvDtls_service_start,
               srvLt_srvDtls_service_end,
               srvLi_srvDtls_employee_id,
               srvLs_srvDtls_employee_name,
               srvLi_srvDtls_booking_status,
               srvLt_srvDtls_status_date,
               srvLi_srvDtls_service_quantity,
               srvLs_srvDtls_service_description        
               );
END LOOP cur_inner_loop;
END BLOCK2;
   end loop curloop;
   SET @sql_stmt = 'SELECT ';
   IF in_start_data ='' THEN
       SET  @sql_stmt = CONCAT(@sql_stmt,' * ');
   ELSE
       SET  @sql_stmt = CONCAT(@sql_stmt,' ',in_start_data ,' ');
   END IF;
   
   SET @sql_stmt = CONCAT(@sql_stmt,' FROM app_temp_booking_repo LEFT JOIN vw_customerdetails  ON app_temp_booking_repo.customer_id =vw_customerdetails.user_id WHERE 1=1 ');
   IF in_string_data !='' THEN
       SET @sql_stmt =  CONCAT(@sql_stmt,' ',in_string_data);
   END IF;
   PREPARE statementExecute FROM @sql_stmt;
   EXECUTE statementExecute;
   DROP TEMPORARY TABLE IF EXISTS app_temp_booking;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`bookient`@`localhost` PROCEDURE `staffBlockDetails`(
	IN locl_admin INT,
	IN time_zone varchar(15),
	IN in_string_data varchar(255),
	IN in_start_data varchar(255)
)
BEGIN

   DECLARE li_employee_id INT;
   DECLARE ld_block_date DATE;
   DECLARE ls_is_active varchar(2);
   DECLARE first_cursor boolean;
   
   DECLARE bookingTab_cursor CURSOR FOR


           SELECT    
               bkDt.employee_id AS employee_id,
               bkDt.block_date AS block_date,
               emp.is_active AS is_active
           FROM    
               app_staff_unavailable AS bkDt,
               app_employee AS emp

           WHERE    
               emp.employee_id = bkDt.employee_id
               AND
               emp.local_admin_id = locl_admin
           ORDER BY
               bkDt.block_date;

   DECLARE continue handler for not found set first_cursor = true;
OPEN bookingTab_cursor;

DROP TEMPORARY TABLE IF EXISTS app_temp_blocking;

CREATE TEMPORARY TABLE IF NOT EXISTS `app_temp_blocking` (
                                   `unavailable_time_id` int(11) NULL,
                                   `employee_id` int(11) NOT NULL,
                                   `block_date` DATE NOT NULL,
                                   `time_form` TIME NOT NULL,
                                   `time_to` TIME NOT NULL,
                                   `is_active` varchar(20) NOT NULL
                                    ) ENGINE=HEAP DEFAULT CHARSET=latin1;

   curloop: loop

       fetch bookingTab_cursor into
                               li_employee_id,
                               ld_block_date,
                               ls_is_active ;

       if first_cursor then
           close bookingTab_cursor;
           set first_cursor = false;
           leave curloop;
       end if;
   INSERT INTO
       app_temp_blocking (
               unavailable_time_id,
               employee_id,
               block_date,
               time_form,
               time_to,
               is_active
               )values(
               0,
               li_employee_id,
               ld_block_date,
               '00:00:00',
               '24:00:00',
               ls_is_active        
               );
   end loop curloop;
   
BLOCK2: BEGIN

   DECLARE timLi_unavailable_time_id INT;
   DECLARE timLi_employee_id INT;
   DECLARE timLd_date DATE;
   DECLARE timLs_is_active varchar(2);
   DECLARE timLt_time_form TIME;
   DECLARE timLt_time_to TIME;
   DECLARE timLt_temp_time_form TIME;
   DECLARE timLt_temp_time_to TIME;
   DECLARE timLi_temp_unavailable_time_id INT;
   DECLARE inner_done boolean;
   DECLARE innerCursor CURSOR FOR


           SELECT
               bkTm.unavailable_time_id unavailable_time_id,
               bkTm.employee_id employee_id,
               bkTm.date date,
               ADDTIME(bkTm.time_form,time_zone) time_form,
               ADDTIME(bkTm.time_to,time_zone) time_to,
               emp.is_active is_active
           FROM
               app_staff_unavailable_time bkTm,
               app_employee emp
           WHERE
               emp.employee_id = bkTm.employee_id
               AND
               emp.local_admin_id = locl_admin
               AND
               bkTm.continuation_id  = 0
           ORDER BY
               bkTm.date;
               


DECLARE CONTINUE HANDLER FOR NOT FOUND SET inner_done = TRUE;
OPEN innerCursor ;
   cur_inner_loop: LOOP

       FETCH FROM innerCursor INTO
                   timLi_unavailable_time_id,
                   timLi_employee_id,
                   timLd_date,
                   timLt_time_form,
                   timLt_time_to,
                   timLs_is_active;  

       
       IF inner_done THEN
           CLOSE innerCursor ;
           LEAVE cur_inner_loop;
       END IF;
       
       SET timLt_temp_time_form    = timLt_time_form;
       SET timLt_temp_time_to        = timLt_time_to;
       SET timLi_temp_unavailable_time_id = timLi_unavailable_time_id;
       

BLOCK3: BEGIN
       DECLARE child boolean;
       DECLARE no_of_row INT DEFAULT 0;
       DECLARE child_time_form, child_time_to TIME;
       DECLARE final_time_form, final_time_to TIME;
       DECLARE final_date DATE;
       DECLARE first_value INT;
       DECLARE childCursor CURSOR FOR

   SELECT
       ADDTIME(time_form,time_zone) time_form,
       ADDTIME(time_to,time_zone) time_to
   FROM
       app_staff_unavailable_time
   WHERE
       continuation_id  = timLi_temp_unavailable_time_id;

       
DECLARE CONTINUE HANDLER FOR NOT FOUND SET child = TRUE;

OPEN childCursor ;
   cur_child_loop: LOOP
       FETCH FROM childCursor INTO
                   child_time_form,
                   child_time_to ;  
       
       IF child THEN
           CLOSE childCursor ;
           LEAVE cur_child_loop;
       END IF;
           SET  no_of_row = no_of_row +1;
   END LOOP cur_child_loop;


   IF no_of_row =0 THEN
         SET final_time_form    = timLt_temp_time_form;
         SET final_time_to     =ADDTIME(CAST('00:00:01' as TIME),timLt_temp_time_to);
    ELSE
         SET final_time_form    = timLt_temp_time_form;
         SET final_time_to     = ADDTIME(CAST('00:00:01' as TIME),child_time_to);
    END IF;


   SET first_value = SUBSTRING_INDEX(final_time_form, ':', 1 );
   IF first_value < 0 THEN
       SET final_time_form  =  ADDTIME(CAST('24:00:00' as TIME),final_time_form);
       SET final_date = DATE_SUB(CAST(timLd_date as DATE),INTERVAL 1 DAY);
   ELSEIF first_value > 24 THEN
       SET final_time_form  =  SUBTIME(final_time_form, CAST('24:00:00' as TIME));  
       SET final_date = DATE_ADD(CAST(timLd_date as DATE),INTERVAL 1 DAY);
   END IF;

   

   SET first_value = SUBSTRING_INDEX(final_time_to, ':', 1 );
   IF first_value < 0 THEN
       SET final_time_to  =  ADDTIME(CAST('24:00:00' as TIME),final_time_to);              
   ELSEIF first_value  > 24 THEN
       SET final_time_to  =  SUBTIME(final_time_to, CAST('24:00:00' as TIME));
   END IF;

       

   INSERT INTO
       app_temp_blocking (
               unavailable_time_id,
               employee_id,
               block_date,
               time_form,
               time_to,
               is_active
               )values(
               timLi_unavailable_time_id,
               timLi_employee_id,
               timLd_date,
               final_time_form,
               final_time_to,
               timLs_is_active        
               );

END BLOCK3;
END LOOP cur_inner_loop;
END BLOCK2;

   SET @sql_stmt = 'SELECT ';
   IF in_start_data ='' THEN
       SET  @sql_stmt = CONCAT(@sql_stmt,' * ');
   ELSE
       SET  @sql_stmt = CONCAT(@sql_stmt,' ',in_start_data ,' ');
   END IF;
   
   SET @sql_stmt = CONCAT(@sql_stmt,' FROM app_temp_blocking WHERE 1=1 ');
   IF in_string_data !='' THEN
       SET @sql_stmt =  CONCAT(@sql_stmt,' ',in_string_data);
   END IF;
   SET @sql_stmt = CONCAT(@sql_stmt,' ORDER BY block_date ');
   
   PREPARE statementExecute FROM @sql_stmt;
   EXECUTE statementExecute;

   DROP TEMPORARY TABLE IF EXISTS app_temp_blocking;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`bookient`@`localhost` PROCEDURE `staffBlockDetailsFront`(
	IN locl_admin INT,
	IN time_zone varchar(15),
	IN in_string_data varchar(255),
	IN in_start_data varchar(255)
)
BEGIN
   DECLARE li_employee_id INT;
   DECLARE ld_block_date DATE;
   DECLARE ls_is_active varchar(2);
   DECLARE first_cursor boolean;
   
   DECLARE bookingTab_cursor CURSOR FOR
           SELECT    
               bkDt.employee_id AS employee_id,
               bkDt.block_date AS block_date,
               emp.is_active AS is_active
           FROM    
               app_staff_unavailable AS bkDt,
               app_employee AS emp

           WHERE    
               emp.employee_id = bkDt.employee_id
               AND
               emp.local_admin_id = locl_admin
           ORDER BY
               bkDt.block_date;
   DECLARE continue handler for not found set first_cursor = true;
OPEN bookingTab_cursor;
DROP TEMPORARY TABLE IF EXISTS app_temp_blocking;

CREATE TEMPORARY TABLE IF NOT EXISTS `app_temp_blocking` (
                                   `unavailable_time_id` int(11) NULL,
                                   `employee_id` int(11) NOT NULL,
                                   `block_date` DATE NOT NULL,
                                   `time_form` TIME NOT NULL,
                                   `time_to` TIME NOT NULL,
                                   `is_active` varchar(20) NOT NULL
                                    ) ENGINE=HEAP DEFAULT CHARSET=latin1;
   curloop: loop
       fetch bookingTab_cursor into
                               li_employee_id,
                               ld_block_date,
                               ls_is_active ;
       if first_cursor then
           close bookingTab_cursor;
           set first_cursor = false;
           leave curloop;
       end if;
   INSERT INTO
       app_temp_blocking (
               unavailable_time_id,
               employee_id,
               block_date,
               time_form,
               time_to,
               is_active
               )values(
               0,
               li_employee_id,
               ld_block_date,
               '00:00:00',
               '23:59:59',
               ls_is_active        
               );
   end loop curloop;
   
BLOCK2: BEGIN
   DECLARE timLi_unavailable_time_id INT;
   DECLARE timLi_employee_id INT;
   DECLARE timLd_date DATE;
   DECLARE timLs_is_active varchar(2);
   DECLARE timLt_time_form TIME;
   DECLARE timLt_time_to TIME;
   DECLARE timLt_temp_time_form TIME;
   DECLARE timLt_temp_time_to TIME;
   DECLARE timLi_temp_unavailable_time_id INT;
   DECLARE inner_done boolean;
   DECLARE innerCursor CURSOR FOR
           SELECT
               bkTm.unavailable_time_id unavailable_time_id,
               bkTm.employee_id employee_id,
               bkTm.date date,
               ADDTIME(bkTm.time_form,time_zone) time_form,
               ADDTIME(bkTm.time_to,time_zone) time_to,
               emp.is_active is_active
           FROM
               app_staff_unavailable_time bkTm,
               app_employee emp
           WHERE
               emp.employee_id = bkTm.employee_id
               AND
               emp.local_admin_id = locl_admin
               AND
               bkTm.continuation_id  = 0
           ORDER BY
               bkTm.date;
               

DECLARE CONTINUE HANDLER FOR NOT FOUND SET inner_done = TRUE;
OPEN innerCursor ;
   cur_inner_loop: LOOP
       FETCH FROM innerCursor INTO
                   timLi_unavailable_time_id,
                   timLi_employee_id,
                   timLd_date,
                   timLt_time_form,
                   timLt_time_to,
                   timLs_is_active;  
       
       IF inner_done THEN
           CLOSE innerCursor ;
           LEAVE cur_inner_loop;
       END IF;
       
       SET timLt_temp_time_form    = timLt_time_form;
       SET timLt_temp_time_to        = timLt_time_to;
       SET timLi_temp_unavailable_time_id = timLi_unavailable_time_id;
       
BLOCK3: BEGIN
       DECLARE child boolean;
       DECLARE no_of_row INT DEFAULT 0;
       DECLARE child_time_form, child_time_to TIME;
       DECLARE final_time_form, final_time_to TIME;
       DECLARE final_date DATE;
       DECLARE first_value INT;
       DECLARE childCursor CURSOR FOR
   SELECT
       ADDTIME(time_form,time_zone) time_form,
       ADDTIME(time_to,time_zone) time_to
   FROM
       app_staff_unavailable_time
   WHERE
       continuation_id  = timLi_temp_unavailable_time_id;
       
DECLARE CONTINUE HANDLER FOR NOT FOUND SET child = TRUE;
OPEN childCursor ;
   cur_child_loop: LOOP
       FETCH FROM childCursor INTO
                   child_time_form,
                   child_time_to ;  
       
       IF child THEN
           CLOSE childCursor ;
           LEAVE cur_child_loop;
       END IF;
           SET  no_of_row = no_of_row +1;
   END LOOP cur_child_loop;
   IF no_of_row =0 THEN
         SET final_time_form    = timLt_temp_time_form;
         SET final_time_to     =ADDTIME(CAST('00:00:01' as TIME),timLt_temp_time_to);
    ELSE
         SET final_time_form    = timLt_temp_time_form;
         SET final_time_to     = ADDTIME(CAST('00:00:01' as TIME),child_time_to);
    END IF;
   SET first_value = SUBSTRING_INDEX(final_time_form, ':', 1 );
   IF first_value < 0 THEN
       SET final_time_form  =  ADDTIME(CAST('23:59:59' as TIME),final_time_form);
       SET final_date = DATE_SUB(CAST(timLd_date as DATE),INTERVAL 1 DAY);
   ELSEIF first_value > 24 THEN
       SET final_time_form  =  SUBTIME(final_time_form, CAST('23:59:59' as TIME));  
       SET final_date = DATE_ADD(CAST(timLd_date as DATE),INTERVAL 1 DAY);
   END IF;
   
   SET first_value = SUBSTRING_INDEX(final_time_to, ':', 1 );
   IF first_value < 0 THEN
       SET final_time_to  =  ADDTIME(CAST('23:59:59' as TIME),final_time_to);              
   ELSEIF first_value  > 24 THEN
       SET final_time_to  =  SUBTIME(final_time_to, CAST('23:59:59' as TIME));
   END IF;
       
   INSERT INTO
       app_temp_blocking (
               unavailable_time_id,
               employee_id,
               block_date,
               time_form,
               time_to,
               is_active
               )values(
               timLi_unavailable_time_id,
               timLi_employee_id,
               timLd_date,
               final_time_form,
               final_time_to,
               timLs_is_active        
               );
END BLOCK3;
END LOOP cur_inner_loop;
END BLOCK2;
   SET @sql_stmt = 'SELECT ';
   IF in_start_data ='' THEN
       SET  @sql_stmt = CONCAT(@sql_stmt,' * ');
   ELSE
       SET  @sql_stmt = CONCAT(@sql_stmt,' ',in_start_data ,' ');
   END IF;
   
   SET @sql_stmt = CONCAT(@sql_stmt,' FROM app_temp_blocking WHERE 1=1 ');
   IF in_string_data !='' THEN
       SET @sql_stmt =  CONCAT(@sql_stmt,' ',in_string_data);
   END IF;
   SET @sql_stmt = CONCAT(@sql_stmt,' ORDER BY employee_id ');
   
   PREPARE statementExecute FROM @sql_stmt;
   EXECUTE statementExecute;
   DROP TEMPORARY TABLE IF EXISTS app_temp_blocking;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`bookient`@`localhost` PROCEDURE `staffBlockDetailsFrontWihService`(
	IN locl_admin INT,
	IN time_zone varchar(15),
	IN in_string_data varchar(255),
	IN in_start_data varchar(255)
)
BEGIN
   DECLARE li_employee_id INT;
   DECLARE ld_block_date DATE;
   DECLARE ls_is_active varchar(2);
   DECLARE first_cursor boolean;
   DECLARE new_employee_id INT;
   
   DECLARE bookingTab_cursor CURSOR FOR
           SELECT    
               bkDt.employee_id AS employee_id,
               bkDt.block_date AS block_date,
               emp.is_active AS is_active
           FROM    
               app_staff_unavailable AS bkDt,
               app_employee AS emp

           WHERE    
               emp.employee_id = bkDt.employee_id
               AND
               emp.local_admin_id = locl_admin
           ORDER BY
               bkDt.block_date;
   DECLARE continue handler for not found set first_cursor = true;
OPEN bookingTab_cursor;
DROP TEMPORARY TABLE IF EXISTS app_temp_blocking_local;

CREATE TEMPORARY TABLE IF NOT EXISTS `app_temp_blocking_local` (
                                   `unavailable_time_id` int(11) NULL,
                                   `employee_id` int(11) NOT NULL,
                                   `services_ids` varchar(256) NOT NULL,
                                   `block_date` DATE NOT NULL,
                                   `time_form` TIME NOT NULL,
                                   `time_to` TIME NOT NULL,
                                   `is_active` varchar(20) NOT NULL
                                    ) ENGINE=HEAP DEFAULT CHARSET=latin1;
   curloop: loop
       fetch bookingTab_cursor into
                               li_employee_id,
                               ld_block_date,
                               ls_is_active ;
       if first_cursor then
           close bookingTab_cursor;
           set first_cursor = false;
           leave curloop;
       end if;
       
       SET new_employee_id = li_employee_id;
       NEWBLOCK: BEGIN
           DECLARE services_ids TEXT;
           DECLARE temp_service_id varchar(10);
           DECLARE new_service_cur_done boolean;
           DECLARE new_app_biz_hours_cursor CURSOR FOR
           SELECT
           service_id
           FROM
           app_biz_hours  
           WHERE
           employee_id  = new_employee_id
           AND
           local_admin_id = locl_admin;
       
       DECLARE CONTINUE HANDLER FOR NOT FOUND SET new_service_cur_done = TRUE;
           OPEN new_app_biz_hours_cursor ;
           cur_service_loop: LOOP
           FETCH FROM new_app_biz_hours_cursor INTO
                   temp_service_id;  
       
           IF new_service_cur_done THEN
               CLOSE new_app_biz_hours_cursor ;
               LEAVE cur_service_loop;
           END IF;
       
           SET services_ids = 5 ;
           END LOOP cur_service_loop;
       END NEWBLOCK;
       
       
       
       
   INSERT INTO
       app_temp_blocking_local (
               unavailable_time_id,
               employee_id,
               services_ids,
               block_date,
               time_form,
               time_to,
               is_active
               )values(
               0,
               li_employee_id,
               5,
               ld_block_date,
               '00:00:00',
               '23:59:59',
               ls_is_active        
               );
   end loop curloop;
   
BLOCK2: BEGIN
   DECLARE timLi_unavailable_time_id INT;
   DECLARE timLi_employee_id INT;
   DECLARE timLd_date DATE;
   DECLARE timLs_is_active varchar(2);
   DECLARE timLt_time_form TIME;
   DECLARE timLt_time_to TIME;
   DECLARE timLt_temp_time_form TIME;
   DECLARE timLt_temp_time_to TIME;
   DECLARE timLi_temp_unavailable_time_id INT;
   DECLARE inner_done boolean;
   DECLARE innerCursor CURSOR FOR
           SELECT
               bkTm.unavailable_time_id unavailable_time_id,
               bkTm.employee_id employee_id,
               bkTm.date date,
               ADDTIME(bkTm.time_form,time_zone) time_form,
               ADDTIME(bkTm.time_to,time_zone) time_to,
               emp.is_active is_active
           FROM
               app_staff_unavailable_time bkTm,
               app_employee emp
           WHERE
               emp.employee_id = bkTm.employee_id
               AND
               emp.local_admin_id = locl_admin
               AND
               bkTm.continuation_id  = 0
           ORDER BY
               bkTm.date;
               

DECLARE CONTINUE HANDLER FOR NOT FOUND SET inner_done = TRUE;
OPEN innerCursor ;
   cur_inner_loop: LOOP
       FETCH FROM innerCursor INTO
                   timLi_unavailable_time_id,
                   timLi_employee_id,
                   timLd_date,
                   timLt_time_form,
                   timLt_time_to,
                   timLs_is_active;  
       
       IF inner_done THEN
           CLOSE innerCursor ;
           LEAVE cur_inner_loop;
       END IF;
       
       SET timLt_temp_time_form    = timLt_time_form;
       SET timLt_temp_time_to        = timLt_time_to;
       SET timLi_temp_unavailable_time_id = timLi_unavailable_time_id;
       
BLOCK3: BEGIN
       DECLARE child boolean;
       DECLARE no_of_row INT DEFAULT 0;
       DECLARE child_time_form, child_time_to TIME;
       DECLARE final_time_form, final_time_to TIME;
       DECLARE final_date DATE;
       DECLARE first_value INT;
       DECLARE childCursor CURSOR FOR
   SELECT
       ADDTIME(time_form,time_zone) time_form,
       ADDTIME(time_to,time_zone) time_to
   FROM
       app_staff_unavailable_time
   WHERE
       continuation_id  = timLi_temp_unavailable_time_id;
       
DECLARE CONTINUE HANDLER FOR NOT FOUND SET child = TRUE;
OPEN childCursor ;
   cur_child_loop: LOOP
       FETCH FROM childCursor INTO
                   child_time_form,
                   child_time_to ;  
       
       IF child THEN
           CLOSE childCursor ;
           LEAVE cur_child_loop;
       END IF;
           SET  no_of_row = no_of_row +1;
   END LOOP cur_child_loop;
   IF no_of_row =0 THEN
         SET final_time_form    = timLt_temp_time_form;
         SET final_time_to     =ADDTIME(CAST('00:00:01' as TIME),timLt_temp_time_to);
    ELSE
         SET final_time_form    = timLt_temp_time_form;
         SET final_time_to     = ADDTIME(CAST('00:00:01' as TIME),child_time_to);
    END IF;
   SET first_value = SUBSTRING_INDEX(final_time_form, ':', 1 );
   IF first_value < 0 THEN
       SET final_time_form  =  ADDTIME(CAST('23:59:59' as TIME),final_time_form);
       SET final_date = DATE_SUB(CAST(timLd_date as DATE),INTERVAL 1 DAY);
   ELSEIF first_value > 24 THEN
       SET final_time_form  =  SUBTIME(final_time_form, CAST('23:59:59' as TIME));  
       SET final_date = DATE_ADD(CAST(timLd_date as DATE),INTERVAL 1 DAY);
   END IF;
   
   SET first_value = SUBSTRING_INDEX(final_time_to, ':', 1 );
   IF first_value < 0 THEN
       SET final_time_to  =  ADDTIME(CAST('23:59:59' as TIME),final_time_to);              
   ELSEIF first_value  > 24 THEN
       SET final_time_to  =  SUBTIME(final_time_to, CAST('23:59:59' as TIME));
   END IF;
       
   INSERT INTO
       app_temp_blocking_local (
               unavailable_time_id,
               employee_id,
               block_date,
               time_form,
               time_to,
               is_active
               )values(
               timLi_unavailable_time_id,
               timLi_employee_id,
               timLd_date,
               final_time_form,
               final_time_to,
               timLs_is_active        
               );
END BLOCK3;
END LOOP cur_inner_loop;
END BLOCK2;
   SET @sql_stmt = 'SELECT ';
   IF in_start_data ='' THEN
       SET  @sql_stmt = CONCAT(@sql_stmt,' * ');
   ELSE
       SET  @sql_stmt = CONCAT(@sql_stmt,' ',in_start_data ,' ');
   END IF;
   
   SET @sql_stmt = CONCAT(@sql_stmt,' FROM app_temp_blocking_local WHERE 1=1 ');
   IF in_string_data !='' THEN
       SET @sql_stmt =  CONCAT(@sql_stmt,' ',in_string_data);
   END IF;
   SET @sql_stmt = CONCAT(@sql_stmt,' ORDER BY employee_id ');
   
   PREPARE statementExecute FROM @sql_stmt;
   EXECUTE statementExecute;
   DROP TEMPORARY TABLE IF EXISTS app_temp_blocking_local;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-07 13:00:38
