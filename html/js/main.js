(function ($) {
	
/*==============================================================*/
// WoW Animation
/*==============================================================*/
	new WOW().init();
	
$("body").waypoint(function() {
    $(".navbar").toggleClass("navbar__initial");
    return false;
}, { offset: "-20px" });


$(".postslide").each(function() {
  var $this = $(this);
  $this.owlCarousel({
    items : 1, 
        itemsDesktop : [992,1],
        itemsDesktopSmall : [768,1], 
        itemsTablet: [450,1], 
        itemsMobile : false,// itemsMobile disabled - inherit from itemsTablet option
        pagination : false,
        autoPlay:true
  });
  // Custom Navigation Events
  $this.parent().find(".next").click(function(){
    $this.trigger('owl.next');
  });
  $this.parent().find(".prev").click(function(){
    $this.trigger('owl.prev');
  });
});

/*==============================================================*/
// video popup
/*==============================================================*/

$(document).ready(function() {
	$('.popup-youtube, .popup-vimeo').magnificPopup({
	  delegate: 'a',
	  type: 'iframe',
	  mainClass: 'mfp-img-mobile',
	  removalDelay: 160,
	 gallery: {
		enabled: true,
		navigateByImgClick: true,
		preload: [0,1] // Will preload 0 - before current, and 1 after the current image
	  },

	  fixedContentPos: false
	});
});	
$(document).ready(function() {
	$('.mbpopup-youtube, .popup-vimeo').magnificPopup({
	  delegate: 'a',
	  type: 'iframe',
	  mainClass: 'mfp-img-mobile',
	  removalDelay: 160,
	 gallery: {
		enabled: true,
		navigateByImgClick: true,
		preload: [0,1] // Will preload 0 - before current, and 1 after the current image
	  },

	  fixedContentPos: false
	});
});	



})(jQuery); 